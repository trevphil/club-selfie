//
//  Club+Create.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Club+Create.h"

@implementation Club (Create)

+ (Club *)createClubInContext:(NSManagedObjectContext *)context {
    Club *club = [NSEntityDescription insertNewObjectForEntityForName:@"Club"
                                               inManagedObjectContext:context];
    club.unique = [@([NSDate timeIntervalSinceReferenceDate]) stringValue];
    club.createdAt = [NSDate date];
    club.lastActiveAt = [NSDate date];
    club.title = [NSString stringWithFormat:@"Random Club %d", arc4random() % 100];
    return club;
}

@end
