//
//  PhotoCell.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/23/14.
//
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UITableViewCell

- (UILabel *)userLabel;
- (UILabel *)ratingLabel;
- (UILabel *)commentsLabel;

@property (weak, nonatomic) UIImage *thumbnail;

- (UIImage *)photo;
- (void)setPhoto:(UIImage *)photo;

@end
