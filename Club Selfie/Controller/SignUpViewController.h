//
//  SignUpViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "UserFormViewController.h"

@interface SignUpViewController : UserFormViewController

// setting up an account
@property (nonatomic) NSInteger minUsernameLength; // default is 4
@property (nonatomic) NSInteger minPasswordLength; // default is 6
@property (nonatomic) BOOL passwordRequiresNumber; // default is YES
@property (nonatomic) BOOL passwordRequiresCapitalLetter; // default is NO

// YOU MUST IMPLEMENT THE SIGN UP MECHANISM
- (void)signUpWithEmail:(NSString *)emailAddress
           withUsername:(NSString *)username
           withPassword:(NSString *)password;

@end
