//
//  AUIAutoGrowingTextView.m
//
//  Created by Adam on 10/10/13.
//

#import "ExpandingTextView.h"

@interface ExpandingTextView()

@property (strong, nonatomic) NSLayoutConstraint *heightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *maxHeightConstraint;
@property (strong, nonatomic) NSLayoutConstraint *minHeightConstraint;

@end

@implementation ExpandingTextView

#pragma mark - Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (void)setup {
    //self.maxHeight = 150.0f;
    //self.minHeight = 28.0f;
    
    // If we are using auto layouts, than get a handler to the height constraint.
    for (NSLayoutConstraint *constraint in self.constraints) {
        if (constraint.firstAttribute == NSLayoutAttributeHeight) {
            self.heightConstraint = constraint;
            break;
        }
    }
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.heightConstraint) {
        [self handleLayoutWithAutoLayouts];
    }
    
    // Center vertically
    // We're  supposed to have a maximum height constraint in code for the text view
    // which will makes the intrinsicSize eventually higher then the height of the text view,
    // if we had enough text. This code only vertically centers the textView while the
    // context size is smaller/equal to the textView's frame.
    /*if (self.intrinsicContentSize.height <= self.bounds.size.height) {
        CGFloat topCorrect = (self.bounds.size.height - self.contentSize.height * self.zoomScale) / 2.0f;
        topCorrect = topCorrect < 0 ? 0 : topCorrect;
        self.contentOffset = CGPointMake(0, -topCorrect);
    }*/
}

- (void)handleLayoutWithAutoLayouts {
    CGSize intrinsicSize = self.contentSize;
    if (self.minHeight) {
        intrinsicSize.height = MAX(intrinsicSize.height, self.minHeight);
    }
    if (self.maxHeight) {
        intrinsicSize.height = MIN(intrinsicSize.height, self.maxHeight);
    }
    self.heightConstraint.constant = intrinsicSize.height;
}

@end
