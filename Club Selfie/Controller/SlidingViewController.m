//
//  SlidingViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/10/14.
//
//

#import "EntryScreenViewController.h"
#import "SlidingViewController.h"
#import "ColorHelper.h"
#import "User+Create.h"
#import "SettingsTVC.h"
#import "FriendsVC.h"
#import "ClubsVC.h"
#import "PostsVC.h"

@interface SlidingViewController ()

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (weak, nonatomic) IBOutlet UIView *tutorialQuestionView;

@end

@implementation SlidingViewController

#pragma mark - Helper Methods

- (PostsVC *)postsVC {
    return ((UINavigationController *)self.topViewController).viewControllers[0];
}

- (void)passContextToViewControllers {
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USER_ID_KEY];
    User *currentUser = [User currentUserWithID:currentUserID inContext:self.context]; // make sure we have the current user
    UITabBarController *slideOutTabBarController = (UITabBarController *)self.underLeftViewController;
    ClubsVC *clubsVC = ((UINavigationController *)slideOutTabBarController.viewControllers[0]).viewControllers[0];
    clubsVC.context = currentUser.managedObjectContext;
    FriendsVC *friendsVC = ((UINavigationController *)slideOutTabBarController.viewControllers[1]).viewControllers[0];
    friendsVC.context = currentUser.managedObjectContext;
    SettingsTVC *settingsTVC = ((UINavigationController *)slideOutTabBarController.viewControllers[2]).viewControllers[0];
    settingsTVC.context = currentUser.managedObjectContext;
    
    NSString *lastViewedClubID = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_VIEWED_CLUB_KEY];
    if ([lastViewedClubID length]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique = %@", lastViewedClubID];
        [self postsVC].club = [[[User currentUser].clubs filteredSetUsingPredicate:predicate] anyObject];
    }
}

- (void)checkForInitialSetup {
    NSString *currentUserID = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USER_ID_KEY];
    BOOL noCachedUser = [currentUserID length] ? NO : YES;
    BOOL firstLaunch = [((NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:FIRST_LAUNCH_KEY]) boolValue];
    if (noCachedUser) {
        UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        UINavigationController *entryNav = (UINavigationController *)[loginStoryboard instantiateInitialViewController];
        EntryScreenViewController *entryVC = entryNav.viewControllers[0];
        entryVC.context = self.context;
        [self presentViewController:entryNav animated:YES completion:NULL];
    } else if (firstLaunch) {
        UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"Tutorial" bundle:nil];
        UIViewController *tutorialNav = [tutorialStoryboard instantiateInitialViewController];
        [self presentViewController:tutorialNav animated:YES completion:NULL];
    }
}

#pragma mark - Tutorial

- (void)setupTutorialQuestionView {
    [self.view bringSubviewToFront:self.tutorialQuestionView];
    self.tutorialQuestionView.backgroundColor = [UIColor whiteColor];
    self.tutorialQuestionView.opaque = NO;
    self.tutorialQuestionView.layer.cornerRadius = 5.0f;
    self.tutorialQuestionView.layer.borderColor = [[ColorHelper mediumGreen] CGColor];
    self.tutorialQuestionView.layer.borderWidth = 2.0f;
    self.tutorialQuestionView.hidden = YES;
}

- (void)showTutorial {
    UIStoryboard *tutorialStoryboard = [UIStoryboard storyboardWithName:@"Tutorial" bundle:nil];
    UIViewController *tutorialNav = [tutorialStoryboard instantiateInitialViewController];
    [self presentViewController:tutorialNav animated:YES completion:^{
        self.tutorialQuestionView.hidden = YES;
    }];
}

- (void)showTutorialIfNeeded {
    [self showTutorialIfNeeded:NO];
}

- (void)showTutorialIfNeeded:(BOOL)force {
    if (force) {
        [self showTutorial];
        return;
    }
    
    BOOL firstLaunch = [((NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:FIRST_LAUNCH_KEY]) boolValue];
    if (firstLaunch) {
        self.tutorialQuestionView.hidden = NO;
        CGRect frame = self.tutorialQuestionView.frame;
        frame.origin.y = -frame.size.height;
        self.tutorialQuestionView.frame = frame;
        [UIView animateWithDuration:1.0
                              delay:0
             usingSpringWithDamping:0.50
              initialSpringVelocity:0.3
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.tutorialQuestionView.center = self.view.center;
                         }
                         completion:NULL];
    }
}

- (IBAction)noTutorialButtonPressed:(UIButton *)sender {
    [UIView animateWithDuration:0.50
                     animations:^{
                         CGRect frame = self.tutorialQuestionView.frame;
                         frame.origin.y = -frame.size.height;
                         self.tutorialQuestionView.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         self.tutorialQuestionView.hidden = YES;
                     }];
}

- (IBAction)yesTutorialButtonPressed:(UIButton *)sender {
    [self showTutorial];
}

#pragma mark - View Controller Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    [[NSNotificationCenter defaultCenter] addObserverForName:DatabaseAvailabilityNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      self.context = [note.userInfo objectForKey:DatabaseAvailabilityContext];
                                                      [self checkForInitialSetup];
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:CurrentUserAvailableNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [self passContextToViewControllers];
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:CurrentClubDeletedNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [self postsVC].club = nil;
                                                  }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGesturePanning | ECSlidingViewControllerAnchoredGestureTapping;
    self.anchorRightPeekAmount = 44.0f;
    
    [self setupTutorialQuestionView];
}

@end
