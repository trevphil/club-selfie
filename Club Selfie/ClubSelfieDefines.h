//
//  ClubSelfieDefines.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/15/14.
//
//

#ifndef Club_Selfie_ClubSelfieDefines_h
#define Club_Selfie_ClubSelfieDefines_h

// key-value pair keys
#define FIRST_LAUNCH_KEY @"Club Selfie First Time Launch"
#define LAST_VIEWED_CLUB_KEY @"Last Viewed Club ID"
#define CURRENT_USER_ID_KEY @"Club Selfie Current User ID"

// prototype cell identifiers
#define CLUB_CELL_IDENTIFIER @"Club Cell"
#define USER_CELL_IDENTIFIER @"User Cell"
#define PHOTO_CELL_IDENTIFIER @"Photo Cell"
#define COMMENT_CELL_IDENTIFIER @"Comment"

// segue identifiers
#define SIGN_UP_SEGUE_IDENTIFIER @"Sign Up Segue"
#define LOGIN_SEGUE_IDENTIFIER @"Login Segue"
#define POST_SEGUE_IDENTIFIER @"Show Post"
#define INFO_SEGUE_IDENTIFIER @"Show Info"

// storyboard view controller identifiers
#define POST_PHOTO_NAVIGATION_VC_STORYBOARD_ID @"Post Photo"
#define ADD_CLUB_NAVIGATION_VC_STORYBOARD_ID @"Add Club Nav"
#define TUTORIAL_CONTENT_STORYBOARD_ID @"Tutorial Content VC"
#define TUTORIAL_PAGE_VC_STORYBOARD_ID @"Tutorial Page VC"

// misc
#define DATABASE_FILE_NAME @"ClubSelfieDatabase"

#endif
