//
//  Post.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/13/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Club, Comment, User;

@interface Post : NSManagedObject

@property (nonatomic, retain) NSNumber * aspectRatio;
@property (nonatomic, retain) NSNumber * averageRating;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * numberOfRatings;
@property (nonatomic, retain) NSData * photoData;
@property (nonatomic, retain) NSString * photoURL;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) Club *club;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) User *poster;
@property (nonatomic, retain) NSSet *usersWhoRated;
@end

@interface Post (CoreDataGeneratedAccessors)

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

- (void)addUsersWhoRatedObject:(User *)value;
- (void)removeUsersWhoRatedObject:(User *)value;
- (void)addUsersWhoRated:(NSSet *)values;
- (void)removeUsersWhoRated:(NSSet *)values;

@end
