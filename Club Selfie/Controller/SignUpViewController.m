//
//  SignUpViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@property (weak, nonatomic) UITextField *emailTextField;
@property (weak, nonatomic) UITextField *usernameTextField;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (weak, nonatomic) UITextField *confirmPasswordTextField;

@end

@implementation SignUpViewController

#pragma mark - Form Submission

- (void)submitForm {
    if ([self allFieldsAreValid]) {
        [self signUpWithEmail:self.emailTextField.text
                 withUsername:self.usernameTextField.text
                 withPassword:self.passwordTextField.text];
    }
}

- (void)signUpWithEmail:(NSString *)emailAddress
           withUsername:(NSString *)username
           withPassword:(NSString *)password {
    User *currentUser = [User createUserWithEmail:emailAddress username:username
                                         password:password context:self.context];
    if (currentUser) {
        [[NSUserDefaults standardUserDefaults] setObject:currentUser.unique forKey:CURRENT_USER_ID_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:CurrentUserAvailableNotification object:nil];
        SlidingViewController *svc = (SlidingViewController *)self.presentingViewController;
        [svc dismissViewControllerAnimated:YES completion:^{
            [svc showTutorialIfNeeded];
        }];
    } else {
        [self displayError:@"Oops! Sign up failed."];
    }
}

#pragma mark - Text Fields

- (UITextField *)emailTextField {
    return self.inputFields[0];
}
- (UITextField *)usernameTextField {
    return self.inputFields[1];
}
- (UITextField *)passwordTextField {
    return self.inputFields[2];
}
- (UITextField *)confirmPasswordTextField {
    return self.inputFields[3];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // the text fields' delegates should be set in the storyboard
    
    [textField resignFirstResponder];
    if ([textField isEqual:self.emailTextField]) {
        if ([self validEmail:self.emailTextField.text]) [self.usernameTextField becomeFirstResponder];
    } else if ([textField isEqual:self.usernameTextField]) {
        if ([self validUsername:self.usernameTextField.text]) [self.passwordTextField becomeFirstResponder];
    } else if ([textField isEqual:self.passwordTextField]) {
        if ([self validPassword:self.passwordTextField.text]) [self.confirmPasswordTextField becomeFirstResponder];
    } else if ([self validConfirmPassword:self.confirmPasswordTextField.text]) {
        [self submitForm];
    }
    
    return YES;
}

#pragma mark - Validating User Input

- (BOOL)validUsername:(NSString *)username {
    if (![username length]) {
        [self displayError:@"You must enter a username."];
        return NO;
    } else if ([username length] < self.minUsernameLength) {
        [self displayError:[NSString stringWithFormat:@"Username must be %d or more characters.", (int)self.minUsernameLength]];
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)validPassword:(NSString *)password {
    if (![password length]) {
        [self displayError:@"You must enter a password."];
        return NO;
    } else if ([password length] < self.minPasswordLength) {
        [self displayError:[NSString stringWithFormat:@"Password must be %d or more characters.", (int)self.minPasswordLength]];
        return NO;
    } else if (self.passwordRequiresCapitalLetter && [[password lowercaseString] isEqualToString:password]) {
        [self displayError:@"Password must contain at least 1 capital letter."];
        return NO;
    } else if (self.passwordRequiresNumber) {
        NSRange range = [password rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
        BOOL foundNumber = range.location == NSNotFound ? NO : YES;
        if (!foundNumber) {
            [self displayError:@"Password must contain at least 1 number."];
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

- (BOOL)validConfirmPassword:(NSString *)confirmation {
    if ([confirmation isEqualToString:self.passwordTextField.text]) {
        return YES;
    } else {
        [self displayError:@"Passwords do not match."];
        return NO;
    }
}

- (BOOL)allFieldsAreValid {
    BOOL allGood = [self validEmail:self.emailTextField.text] &&
    [self validUsername:self.usernameTextField.text] &&
    [self validPassword:self.passwordTextField.text] &&
    [self validConfirmPassword:self.confirmPasswordTextField.text];
    if (allGood) {
        self.errorsTextLabel.text = @"";
    }
    return allGood;
}

@end
