//
//  TutorialContentVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/15/14.
//
//

#import "TutorialContentVC.h"

@interface TutorialContentVC ()

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation TutorialContentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.backgroundImageView.image = [UIImage imageNamed:self.imageFile];
}

@end
