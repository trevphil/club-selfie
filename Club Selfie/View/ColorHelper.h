//
//  ColorHelper.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/29/14.
//
//

#import <Foundation/Foundation.h>

@interface ColorHelper : NSObject

// flat colors
+ (UIColor *)mediumGreen;
+ (UIColor *)gold;
+ (UIColor *)oceanBlue;
+ (UIColor *)tropicalBlue;

// dark to light gradient
+ (NSArray *)bambooGradient;

@end
