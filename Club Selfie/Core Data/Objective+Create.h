//
//  Objective+Create.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import "Objective.h"
#import "Club.h"

@interface Objective (Create)

+ (Objective *)createObjectiveForClub:(Club *)club;

@end
