//
//  AppDelegate.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/14/14.
//
//

#import "AppDelegate.h"
#import "User+Create.h"

@interface AppDelegate ()
@property (strong, nonatomic) NSManagedObjectContext *databaseContext;
@end

@implementation AppDelegate

- (void)checkForFirstLaunch {
    NSString *firstLaunchString = [[NSUserDefaults standardUserDefaults] objectForKey:FIRST_LAUNCH_KEY];
    BOOL isFirstLaunch = !firstLaunchString ? YES : NO;
    if (isFirstLaunch) {
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:FIRST_LAUNCH_KEY];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:FIRST_LAUNCH_KEY];
    }
}

- (void)setDatabaseContext:(NSManagedObjectContext *)databaseContext {
    NSString *cachedUserID = [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USER_ID_KEY];
    User *currentUser = nil;
    if ([cachedUserID length]) { // else, no user (need sign up or login)
        // setup the current user once and once only
        currentUser = [User currentUserWithID:cachedUserID inContext:databaseContext];
        if (currentUser) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CurrentUserAvailableNotification object:nil];
        } else {
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:CURRENT_USER_ID_KEY];
        }
    }
    _databaseContext = currentUser ? currentUser.managedObjectContext : databaseContext;
    NSDictionary *userInfo = self.databaseContext ? @{ DatabaseAvailabilityContext : self.databaseContext } : nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:DatabaseAvailabilityNotification
                                                        object:self
                                                      userInfo:userInfo];
}

#pragma mark - UIManagedDocument

- (void)setupDocument {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsDirectory = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
    NSURL *url = [documentsDirectory URLByAppendingPathComponent:DATABASE_FILE_NAME];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    [self openOrSaveDocument:document withURL:url];
}

- (void)openOrSaveDocument:(UIManagedDocument *)document withURL:(NSURL *)url {
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[url path]];
    if (fileExists) {
        [document openWithCompletionHandler:^(BOOL success) {
            if (success) {
                self.databaseContext = document.managedObjectContext;
            } else {
                NSLog(@"Could not open document at %@", url);
            }
        }];
    } else {
        [document saveToURL:url forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success) {
              if (success) {
                  self.databaseContext = document.managedObjectContext;
              } else {
                  NSLog(@"Could not create document at %@", url);
              }
          }];
    }
}

#pragma mark - UIApplicationDelegate

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self checkForFirstLaunch];
    [self setupDocument];
    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

@end
