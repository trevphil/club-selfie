//
//  ExistingClubInfoVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/10/14.
//
//

#import "ExistingClubInfoVC.h"
#import "AddUsersToClubVC.h"
#import "CustomButton.h"
#import "Comment.h"
#import "Post.h"

#warning legal docs, log-out/log-in, web backend, push notifications, FB/Twitter integration, RESTKit

@interface ExistingClubInfoVC () <UIAlertViewDelegate>

@property (strong, nonatomic) User *selectedUser; // weak?

@end

@implementation ExistingClubInfoVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isAdmin) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                               target:self
                                                                                               action:@selector(editButtonPressed:)];
    } else {
        self.navigationItem.rightBarButtonItem = nil;
        self.tableView.tableFooterView = nil;
    }
    
    self.objectiveTextView.text = self.objective.text;
    
    NSDate *minDate = [self.objective.expiresAt timeIntervalSinceReferenceDate] < [[NSDate date] timeIntervalSinceReferenceDate] ?
    self.objective.expiresAt : [NSDate date];
    self.datePicker.date = self.objective.expiresAt;
    self.datePicker.minimumDate = minDate;
    int oneWeekInSeconds = 7 * 24 * 3600;
    NSDate *maxDate = [[NSDate date] dateByAddingTimeInterval:oneWeekInSeconds];
    self.datePicker.maximumDate = maxDate;
    [self setupTimer];
    [self updateCountdown];
}

#pragma mark - Actions

- (void)editButtonPressed:(UIBarButtonItem *)sender {
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    UIBarButtonSystemItem newItem = self.tableView.editing ? UIBarButtonSystemItemDone : UIBarButtonSystemItemEdit;
    self.dismissKeyboardGesture.enabled = !self.tableView.editing;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:newItem
                                                                                           target:self
                                                                                           action:@selector(editButtonPressed:)];
}

- (IBAction)addButtonPressed:(UIButton *)sender {
    UINavigationController *addClubNav = [self.storyboard instantiateViewControllerWithIdentifier:ADD_CLUB_NAVIGATION_VC_STORYBOARD_ID];
    AddUsersToClubVC *autcvc = (AddUsersToClubVC *)addClubNav.viewControllers[0];
    autcvc.context = self.objective.managedObjectContext;
    autcvc.editingExistingClub = YES;
    autcvc.club = self.club;
    [self presentViewController:addClubNav animated:YES completion:NULL];
}

#pragma mark - Subclass Overrides

- (int)timeLeft {
    return [self.datePicker.date timeIntervalSinceNow];
}

- (BOOL)isAdmin {
    return [self.club.administrators containsObject:[User currentUser]];
}

- (void)updateFetchedResultsController {
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self createFetchRequest]
                                                                        managedObjectContext:[User currentUser].managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

- (void)updateHeaderViewHeight {
    CGRect newFrame = self.tableView.tableHeaderView.frame;
    if (self.datePicker.isHidden) {
        newFrame.size.height -= self.datePickerOriginalHeight;
    } else {
        newFrame.size.height += self.datePickerOriginalHeight;
    }
    UIView *newTableHeader = self.tableView.tableHeaderView;
    newTableHeader.frame = newFrame;
    // have to re-assign the tableHeaderView for some reason (bug?)
    self.tableView.tableHeaderView = newTableHeader;
}

#pragma mark - Core Data

- (NSFetchRequest *)createFetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF", self.club.members];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"username"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    return request;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    self.objective.text = self.objectiveTextView.text;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex && self.selectedUser != nil) {
        [self.club removeMembersObject:self.selectedUser];
        if ([self.club.administrators containsObject:self.selectedUser]) {
            [self.club removeAdministratorsObject:self.selectedUser];
        }
        
        if ([[User currentUser] isEqual:self.selectedUser]) {
            NSManagedObjectContext *context = [User currentUser].managedObjectContext;
            for (Post *post in self.club.posts) {
                for (Comment *comment in post.comments) {
                    [context deleteObject:comment];
                }
                [context deleteObject:post];
            }
            [context deleteObject:self.objective];
            [context deleteObject:self.club];
            self.selectedUser = nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:CurrentClubDeletedNotification object:self];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

- (void)alertViewCancel:(UIAlertView *)alertView {
    self.selectedUser = nil;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
    
    User *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = user.username;
    cell.imageView.image = [User thumbnailOfUser:user];
    if ([self.club.administrators containsObject:user]) {
        [self addAdminKeyToCell:cell];
    } else {
        cell.accessoryView = nil;
    }
    
    return cell;
}

- (void)addAdminKeyToCell:(UITableViewCell *)cell {
    CGFloat buttonHeight = cell.bounds.size.height * 0.71;
    CGSize buttonSize = CGSizeMake(buttonHeight * 1.50, buttonHeight);
    CustomButton *button = [[CustomButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
    button.chosen = NO;
    button.customType = CustomButtonTypeAdmin;
    for (UIGestureRecognizer *gr in button.gestureRecognizers) {
        gr.enabled = NO;
    }
    cell.accessoryView = button;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return @"Club Members";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    User *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    self.selectedUser = user;
    
    UIAlertView *alert = nil;
    if ([[User currentUser] isEqual:self.selectedUser]) {
        alert = [[UIAlertView alloc] initWithTitle:@"Leave Club"
                                           message:[NSString stringWithFormat:@"Are you sure you want to leave %@?", self.club.title]
                                          delegate:nil
                                 cancelButtonTitle:@"No"
                                 otherButtonTitles:@"Yes", nil];
    } else if (self.isAdmin) {
        alert = [[UIAlertView alloc] initWithTitle:@"Remove User"
                                           message:[NSString stringWithFormat:@"Are you sure you want to remove %@ from this club?", user.username]
                                          delegate:nil
                                 cancelButtonTitle:@"No"
                                 otherButtonTitles:@"Yes", nil];
    }
    if (alert) {
        alert.delegate = self;
        [alert show];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isAdmin) {
        User *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if ([user isEqual:[User currentUser]]) return YES;
        return [self.club.administrators containsObject:user] ? NO : YES;
    }
    return NO;
}

@end
