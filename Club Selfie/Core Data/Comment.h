//
//  Comment.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Post, User;

@interface Comment : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) User *commenter;
@property (nonatomic, retain) Post *post;

@end
