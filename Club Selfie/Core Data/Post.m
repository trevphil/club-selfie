//
//  Post.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/13/14.
//
//

#import "Post.h"
#import "Club.h"
#import "Comment.h"
#import "User.h"


@implementation Post

@dynamic aspectRatio;
@dynamic averageRating;
@dynamic createdAt;
@dynamic latitude;
@dynamic longitude;
@dynamic numberOfRatings;
@dynamic photoData;
@dynamic photoURL;
@dynamic title;
@dynamic unique;
@dynamic club;
@dynamic comments;
@dynamic poster;
@dynamic usersWhoRated;

@end
