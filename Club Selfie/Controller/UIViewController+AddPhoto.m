//
//  UIViewController+AddPhoto.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIViewController+AddPhoto.h"

@implementation UIViewController (AddPhoto)

- (void)showActionSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Take Photo", @"Choose Existing", nil];
    [actionSheet showInView:[self slidingViewController].view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *uiipc = [[UIImagePickerController alloc] init];
    uiipc.delegate = self;
    uiipc.mediaTypes = @[(NSString *)kUTTypeImage];
    
    switch (buttonIndex) {
        case 0:
            if ([[self class] canAccessSourceType:UIImagePickerControllerSourceTypeCamera]) {
                uiipc.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:uiipc animated:YES completion:NULL];
            } else {
                [self fatalAlert:@"Sorry, this device cannot take a photo."];
            }
            break;
        case 1:
            if ([[self class] canAccessSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
                uiipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:uiipc animated:YES completion:NULL];
            } else {
                [self fatalAlert:@"Sorry, Club Selfie cannot access your photo library."];
            }
            break;
        default:
            break;
    }
}

+ (BOOL)canAccessSourceType:(UIImagePickerControllerSourceType)type {
    if ([UIImagePickerController isSourceTypeAvailable:type]) {
        NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
        if (type == UIImagePickerControllerSourceTypeCamera &&
            ![availableMediaTypes containsObject:(NSString *)kUTTypeImage]) {
            return NO;
        }
        return YES;
    }
    return NO;
}

- (void)fatalAlert:(NSString *)msg {
    [[[UIAlertView alloc] initWithTitle:@"Whoops!"
                                message:msg
                               delegate:nil
                      cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *photo = info[UIImagePickerControllerEditedImage];
    if (!photo) photo = info[UIImagePickerControllerOriginalImage];
    if (photo) {
        [self pickedPhoto:photo];
    }
}

- (void)pickedPhoto:(UIImage *)photo {
    // implement when used
}

@end
