//
//  AppDelegate.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/14/14.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
