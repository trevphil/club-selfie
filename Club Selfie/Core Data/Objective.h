//
//  Objective.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Club;

@interface Objective : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * expiresAt;
@property (nonatomic, retain) NSDate * modifiedAt;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) Club *club;

@end
