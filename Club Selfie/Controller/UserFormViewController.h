//
//  UserFormViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "SlidingViewController.h"
#import "User+Create.h"

@interface UserFormViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *context;

// use this if you want to show the user something went wrong (e.g. "Username already taken.")
- (void)displayError:(NSString *)msg;
- (void)alertWithTitle:(NSString *)title message:(NSString *)msg;

// Don't worry about these properties and methods. They're for Login and Sign Up subclasses
@property (weak, nonatomic) IBOutlet UILabel *errorsTextLabel;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *inputFields;
- (BOOL)validEmail:(NSString *)emailAddress;
- (BOOL)allFieldsAreValid;
- (void)submitForm;

@end
