//
//  GradientButton.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/29/14.
//
//

#import "GradientButton.h"
#import "UIView+Styler.h"

@implementation GradientButton

- (void)setup {
    [self bambooGradientBackground];
    [self roundCorners];
}

#pragma mark - Initialization

- (void)awakeFromNib {
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

@end
