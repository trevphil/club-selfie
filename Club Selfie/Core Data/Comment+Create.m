//
//  Comment+Create.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Comment+Create.h"

@implementation Comment (Create)

+ (Comment *)createComment:(NSString *)text inContext:(NSManagedObjectContext *)context {
    Comment *comment = [NSEntityDescription insertNewObjectForEntityForName:@"Comment"
                                                     inManagedObjectContext:context];
    comment.createdAt = [NSDate date];
    comment.text = text;
    return comment;
}

+ (Comment *)createCommentInContext:(NSManagedObjectContext *)context {
    return [self createComment:[NSString stringWithFormat:@"Random comment %d", arc4random() % 100]
                     inContext:context];
}

@end
