//
//  User+Create.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "User.h"

@interface User (Create)

+ (UIImage *)thumbnailOfUser:(User *)user;
+ (UIImage *)thumbnailOfUser:(User *)user forSize:(CGSize)size;

// sign up and login
+ (User *)createUserWithEmail:(NSString *)email
                     username:(NSString *)uname
                     password:(NSString *)pass
                      context:(NSManagedObjectContext *)ctxt;
+ (User *)userWithEmail:(NSString *)email
               password:(NSString *)pass
                context:(NSManagedObjectContext *)ctxt;
+ (User *)userWithUsername:(NSString *)uname
                  password:(NSString *)pass
                   context:(NSManagedObjectContext *)ctxt;

+ (User *)currentUser;
+ (User *)currentUserWithID:(NSString *)unique inContext:(NSManagedObjectContext *)context;
+ (User *)userWithID:(NSString *)unique inContext:(NSManagedObjectContext *)context;
+ (void)deleteAllRecords;

@end
