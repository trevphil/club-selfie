//
//  MainScreenViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/19/14.
//
//

#import "CoreDataViewController.h"
#import "Club.h"

@interface PostsVC : CoreDataViewController

@property (strong, nonatomic) Club *club;

- (IBAction)unwindToPostsVC:(UIStoryboardSegue *)segue;

@end
