//
//  Objective+Create.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import "Objective+Create.h"

@implementation Objective (Create)

+ (Objective *)createObjectiveForClub:(Club *)club {
    Objective *objective = [NSEntityDescription insertNewObjectForEntityForName:@"Objective"
                                              inManagedObjectContext:club.managedObjectContext];
    objective.createdAt = [NSDate date];
    objective.modifiedAt = [NSDate date];
    objective.expiresAt = [NSDate dateWithTimeIntervalSinceNow:90.0];
    objective.text = @"Take a selfie with as many people as possible!";
    objective.unique = [@([NSDate timeIntervalSinceReferenceDate]) stringValue];
    return objective;
}

@end
