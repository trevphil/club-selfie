//
//  Objective.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import "Objective.h"
#import "Club.h"


@implementation Objective

@dynamic createdAt;
@dynamic expiresAt;
@dynamic modifiedAt;
@dynamic text;
@dynamic unique;
@dynamic club;

@end
