//
//  User.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/13/14.
//
//

#import "User.h"
#import "Club.h"
#import "Comment.h"
#import "Post.h"
#import "User.h"


@implementation User

@dynamic averagePostRating;
@dynamic email;
@dynamic lastActiveAt;
@dynamic name;
@dynamic password;
@dynamic thumbnailData;
@dynamic thumbnailURL;
@dynamic unique;
@dynamic username;
@dynamic clubs;
@dynamic comments;
@dynamic controlledClubs;
@dynamic friendOf;
@dynamic friends;
@dynamic knownTo;
@dynamic knows;
@dynamic posts;
@dynamic ratedPosts;

@end
