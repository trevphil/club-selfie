//
//  ColorHelper.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/29/14.
//
//

#import "ColorHelper.h"

@implementation ColorHelper

// flat colors
+ (UIColor *)mediumGreen {
    return [[self class] colorWithScaledRed:76.0 green:217.0 blue:100.0];
}

+ (UIColor *)gold {
    return [[self class] colorWithScaledRed:255.0 green:204.0 blue:0];
}

+ (UIColor *)oceanBlue {
    return [[self class] colorWithScaledRed:29.0 green:119.0 blue:239.0];
}

+ (UIColor *)tropicalBlue {
    return [[self class] colorWithScaledRed:85.0 green:239.0 blue:203.0];
}

// dark to light gradient
+ (NSArray *)bambooGradient {
    return @[[[self class] colorWithScaledRed:90.0 green:212.0 blue:39.0],
             [[self class] colorWithScaledRed:164.0 green:231.0 blue:134.0]];
}

+ (UIColor *)colorWithScaledRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue {
    return [UIColor colorWithRed:(red / 255.0) green:(green / 255.0) blue:(blue / 255.0) alpha:1.0];
}

@end
