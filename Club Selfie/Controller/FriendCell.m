//
//  FriendCell.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/12/14.
//
//

#import "FriendCell.h"

@interface FriendCell ()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet CustomButton *addFriendButton;

@end

@implementation FriendCell

- (void)awakeFromNib {
    self.addFriendButton.customType = CustomButtonTypeFriend;
}

- (UIImage *)thumbnail {
    return self.thumbnailView.image;
}

- (void)setThumbnail:(UIImage *)thumbnail {
    self.thumbnailView.image = thumbnail;
}

@end
