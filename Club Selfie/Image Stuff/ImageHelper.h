//
//  ImageHelper.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/22/14.
//
//

#import <Foundation/Foundation.h>

@interface ImageHelper : NSObject

+ (UIImage *)faceCenteredSquareImage:(UIImage *)image;
+ (UIImage *)resizedImage:(UIImage *)image constrainedToWidth:(CGFloat)width;
+ (UIImage *)resizedImage:(UIImage *)image constrainedToHeight:(CGFloat)height;
+ (UIImage *)resizedImage:(UIImage *)image scaledToSize:(CGSize)size;

@end
