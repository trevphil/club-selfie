//
//  PhotoButtonLayerView.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/10/14.
//
//

#import "PhotoButtonLayerView.h"
#import "PhotoButtonOrnament.h"
#import "ColorHelper.h"

@interface PhotoButtonLayerView ()

@property (weak, nonatomic) IBOutlet PhotoButtonOrnament *ornament;
@property (weak, nonatomic) IBOutlet UIToolbar *blurredCircle;

@end

@implementation PhotoButtonLayerView

#pragma mark - Initialization

- (void)awakeFromNib {
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Gestures

// do not respond to gestures unless they are in the button
// otherwise, allow the table view below to receive them
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    CGRect receivingRect = [self convertRect:self.blurredCircle.frame
                                    fromView:self.ornament];
    return CGRectContainsPoint(receivingRect, point) ? YES : NO;
}

- (IBAction)touchDownButton:(UIButton *)sender {
    self.blurredCircle.barTintColor = [ColorHelper gold];
}

- (IBAction)touchUpInsideButton:(UIButton *)sender {
    self.blurredCircle.barTintColor = nil;
}

- (IBAction)touchDragExitButton:(UIButton *)sender {
    self.blurredCircle.barTintColor = nil;
}

#pragma mark - Setup

- (void)setup {
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
    [self setupBlurredCircle];
}

- (void)setupBlurredCircle {
    self.blurredCircle.translucent = YES;
    self.blurredCircle.barTintColor = nil;
    self.blurredCircle.layer.cornerRadius = (self.blurredCircle.bounds.size.width / 2.0f) * 0.99;
    self.blurredCircle.layer.masksToBounds = YES;
}

@end
