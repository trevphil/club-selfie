//
//  SelfiePostTVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/21/14.
//
//

#import "CoreDataViewController.h"
#import "Post.h"

@interface SelfiePostVC : CoreDataViewController

@property (strong, nonatomic) Post *post;

@end
