//
//  CommentCell.h
//  DynamicTable
//
//  Created by Trevor Phillips on 7/14/14.
//  Copyright (c) 2014 Metova. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell

@property (strong, nonatomic) UIImage *thumbnail;
@property (weak, nonatomic, readonly) UIImageView *thumbnailView;
@property (weak, nonatomic, readonly) UILabel *commentLabel;

@end
