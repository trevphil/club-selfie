//
//  TutorialContentVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/15/14.
//
//

#import <UIKit/UIKit.h>

@interface TutorialContentVC : UIViewController

@property (nonatomic) NSUInteger pageIndex;
@property (strong, nonatomic) NSString *imageFile;

@end
