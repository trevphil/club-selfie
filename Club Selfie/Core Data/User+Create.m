//
//  User+Create.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Objective+Create.h"
#import "Comment+Create.h"
#import "User+Create.h"
#import "Club+Create.h"
#import "Post+Create.h"
#import "ImageHelper.h"

@implementation User (Create)

#pragma mark - User Attributes

+ (UIImage *)thumbnailOfUser:(User *)user {
    if (user.thumbnailData) {
        return [UIImage imageWithData:user.thumbnailData];
    } else {
        return [UIImage imageNamed:user.thumbnailURL];
    }
}

+ (UIImage *)thumbnailOfUser:(User *)user forSize:(CGSize)size {
    UIImage *thumbnail = [[self class] thumbnailOfUser:user];
    if (thumbnail.size.width > size.width) {
        thumbnail = [ImageHelper resizedImage:thumbnail constrainedToWidth:size.width];
    }
    if (thumbnail.size.height > size.height) {
        thumbnail = [ImageHelper resizedImage:thumbnail constrainedToHeight:size.height];
    }
    return thumbnail;
}

#pragma mark - Current User

+ (User *)currentUser {
    return [[self class] currentUserWithID:nil inContext:nil];
}

+ (User *)currentUserWithID:(NSString *)unique inContext:(NSManagedObjectContext *)context {
    static User *_currentUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
        request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", unique];
        NSArray *matches = [context executeFetchRequest:request error:NULL];
        _currentUser = [matches count] == 1 ? [matches firstObject] : nil;
    });
    return _currentUser;
}

#pragma mark - Sign Up and Login

+ (User *)createUserWithEmail:(NSString *)email username:(NSString *)uname
                     password:(NSString *)pass context:(NSManagedObjectContext *)ctxt {
    User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                               inManagedObjectContext:ctxt];
    user.unique = [@([[NSDate date] timeIntervalSinceReferenceDate]) stringValue];
    user.email = email;
    user.username = uname;
    user.password = pass;
    user.lastActiveAt = [NSDate date];
    user.thumbnailURL = @"BasicProfile.png";
    [[self class] generateFakeDataForUser:user inContext:ctxt];
    return user;
}

+ (User *)userWithEmail:(NSString *)email password:(NSString *)pass
                context:(NSManagedObjectContext *)ctxt {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"email = %@ AND password = %@", email, pass];
    
    NSArray *matches = [ctxt executeFetchRequest:request error:NULL];
    if (!matches || ([matches count] != 1)) {
        return nil;
    } else {
        return [matches firstObject];
    }
}

+ (User *)userWithUsername:(NSString *)uname password:(NSString *)pass
                   context:(NSManagedObjectContext *)ctxt {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"username = %@ AND password = %@", uname, pass];

    NSArray *matches = [ctxt executeFetchRequest:request error:NULL];
    if (!matches || ([matches count]  != 1)) {
        return nil;
    } else {
        return [matches firstObject];
    }
}

#pragma mark - Finding Users

+ (User *)userWithID:(NSString *)unique inContext:(NSManagedObjectContext *)context {
    User *user = nil;
    
    if ([unique length]) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
        request.predicate = [NSPredicate predicateWithFormat:@"unique = %@", unique];
        
        NSError *error;
        NSArray *matches = [context executeFetchRequest:request error:&error];
        
        if (!matches || ([matches count] > 1)) {
            // handle error
        } else if (![matches count]) {
            user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                                 inManagedObjectContext:context];
            int rand = arc4random() % 10;
            user.unique = unique;
            user.username = [NSString stringWithFormat:@"User %d", rand];
            user.name = [@(rand) stringValue];
            user.email = [NSString stringWithFormat:@"email%d@example.com", rand];
            user.password = [NSString stringWithFormat:@"pass%d", rand * 10];
            user.thumbnailURL = @"smiley.png";
            user.lastActiveAt = [NSDate date];
        } else {
            user = [matches firstObject];
        }
    }
    
    return user;
}

+ (void)generateFakeDataForUser:(User *)user inContext:(NSManagedObjectContext *)context {
    for (int i = 1; i <= 21; i++) {
        User *otherUser = [User userWithID:[@(i) stringValue] inContext:context];
        switch (i % 3) {
            case 0:
                otherUser.username = [NSString stringWithFormat:@"Bob%d", i];
                break;
            case 1:
                otherUser.username = [NSString stringWithFormat:@"Mary%d", i];
                break;
            default:
                otherUser.username = [NSString stringWithFormat:@"Fred%d", i];
                break;
                
        }
        [user addKnowsObject:otherUser];
        if ([otherUser.username length] < 5) {
            [user addFriendsObject:otherUser];
        }
    }
    for (int i = 0; i < 10; i++) {
        Club *club = [Club createClubInContext:context];
        [club addMembersObject:user];
        if (arc4random() % 2) [club addAdministratorsObject:user];
        [club addMembers:user.knows];
        club.objective = [Objective createObjectiveForClub:club];

        int rand = arc4random() % 12;
        for (int j = 0; j < rand; j++) {
            Post *post = [Post createPostInContext:context];
            post.averageRating = @((arc4random() % 100 + 1) / 10.0);
            post.numberOfRatings = @1;
            [user addRatedPostsObject:post];
            post.poster = user;
            post.club = club;
            for (int k = 0; k < arc4random() % 7; k++) {
                Comment *comment = [Comment createCommentInContext:context];
                comment.commenter = user;
                comment.post = post;
            }
        }
    }
}

+ (void)deleteAllRecords {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsDirectory = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] firstObject];
    NSURL *url = [documentsDirectory URLByAppendingPathComponent:DATABASE_FILE_NAME];
    NSError *error;
    [fileManager removeItemAtURL:url error:&error];
    if (error) NSLog(@"deleting database error: %@", error.localizedDescription);
    
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:LAST_VIEWED_CLUB_KEY];
    [[NSNotificationCenter defaultCenter] postNotificationName:CurrentClubDeletedNotification object:nil];
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:CURRENT_USER_ID_KEY];
}

@end
