//
//  AddPhotoViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/14/14.
//
//

#import <UIKit/UIKit.h>
#import "Post+Create.h"
#import "Club.h"

@interface PostPhotoVC : UIViewController

// in
@property (strong, nonatomic) UIImage *photo;
@property (strong, nonatomic) Club *club;

// out
@property (strong, nonatomic) Post *post;

@end
