//
//  LoginViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "UserFormViewController.h"

@interface LoginViewController : UserFormViewController

// YOU MUST IMPLEMENT THE LOGIN & PASSWORD RECOVERY MECHANISMS

// Some logins require a username, others an email address. Your choice.
// Alternatively, the default mechanism detects whether the user entered an email or username & goes from there.
@property (nonatomic, getter = isLoggingInWithEmail) BOOL loggingInWithEmail;
- (void)loginWithEmail:(NSString *)emailAddress
          withPassword:(NSString *)password;
- (void)loginWithUsername:(NSString *)username
             withPassword:(NSString *)password;

- (void)resetPassword:(NSString *)emailAddress;

@end
