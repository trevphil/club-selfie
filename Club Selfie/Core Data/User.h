//
//  User.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/13/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Club, Comment, Post, User;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * averagePostRating;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSDate * lastActiveAt;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSData * thumbnailData;
@property (nonatomic, retain) NSString * thumbnailURL;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *clubs;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSSet *controlledClubs;
@property (nonatomic, retain) NSSet *friendOf;
@property (nonatomic, retain) NSSet *friends;
@property (nonatomic, retain) NSSet *knownTo;
@property (nonatomic, retain) NSSet *knows;
@property (nonatomic, retain) NSSet *posts;
@property (nonatomic, retain) NSSet *ratedPosts;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addClubsObject:(Club *)value;
- (void)removeClubsObject:(Club *)value;
- (void)addClubs:(NSSet *)values;
- (void)removeClubs:(NSSet *)values;

- (void)addCommentsObject:(Comment *)value;
- (void)removeCommentsObject:(Comment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

- (void)addControlledClubsObject:(Club *)value;
- (void)removeControlledClubsObject:(Club *)value;
- (void)addControlledClubs:(NSSet *)values;
- (void)removeControlledClubs:(NSSet *)values;

- (void)addFriendOfObject:(User *)value;
- (void)removeFriendOfObject:(User *)value;
- (void)addFriendOf:(NSSet *)values;
- (void)removeFriendOf:(NSSet *)values;

- (void)addFriendsObject:(User *)value;
- (void)removeFriendsObject:(User *)value;
- (void)addFriends:(NSSet *)values;
- (void)removeFriends:(NSSet *)values;

- (void)addKnownToObject:(User *)value;
- (void)removeKnownToObject:(User *)value;
- (void)addKnownTo:(NSSet *)values;
- (void)removeKnownTo:(NSSet *)values;

- (void)addKnowsObject:(User *)value;
- (void)removeKnowsObject:(User *)value;
- (void)addKnows:(NSSet *)values;
- (void)removeKnows:(NSSet *)values;

- (void)addPostsObject:(Post *)value;
- (void)removePostsObject:(Post *)value;
- (void)addPosts:(NSSet *)values;
- (void)removePosts:(NSSet *)values;

- (void)addRatedPostsObject:(Post *)value;
- (void)removeRatedPostsObject:(Post *)value;
- (void)addRatedPosts:(NSSet *)values;
- (void)removeRatedPosts:(NSSet *)values;

@end
