//
//  SlideOutTabBarController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/15/14.
//
//

#import <UIKit/UIKit.h>

@interface SlideOutTabBarController : UITabBarController

- (IBAction)unwindToTabBarController:(UIStoryboardSegue *)segue;

@end
