//
//  ClubsVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/22/14.
//
//

#import "CoreDataViewController.h"

@interface ClubsVC : CoreDataViewController

@property (strong, nonatomic) NSManagedObjectContext *context;

@end
