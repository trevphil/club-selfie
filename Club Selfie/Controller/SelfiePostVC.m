//
//  SelfiePostTVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/21/14.
//
//

#import <MobileCoreServices/UTCoreTypes.h>
#import "Comment+Create.h"
#import "SelfiePostVC.h"
#import "ImageHelper.h"
#import "CommentCell.h"
#import "User+Create.h"
#import "Objective.h"
#import "Club.h"

@interface SelfiePostVC () <UITextViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) CommentCell *prototypeCell;

// table header view
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) UIImage *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UILabel *averageRatingLabel;
@property (weak, nonatomic) IBOutlet UISlider *ratingSlider;
@property (weak, nonatomic) IBOutlet UILabel *sliderRatingLabel;
@property (weak, nonatomic) IBOutlet UIButton *rateButton;

// SMS-style texting bar
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@property (weak, nonatomic) IBOutlet UIScrollView *kbAvoidingScrollView;
@property (strong, nonatomic) IBOutlet UIPanGestureRecognizer *fastPan;

@end

@implementation SelfiePostVC

static NSString * const placeholderText = @"Leave a comment...";

#pragma mark - Properties

- (void)setPost:(Post *)post {
    _post = post;
    if ([self.post.title length]) {
        self.title = self.post.title;
    } else {
        self.title = @"Post";
    }
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self createFetchRequest]
                                                                        managedObjectContext:post.managedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

- (UIImage *)thumbnail {
    return self.thumbnailView.image;
}

- (void)setThumbnail:(UIImage *)thumbnail {
    self.thumbnailView.image = thumbnail;
}

- (CommentCell *)prototypeCell {
    if (!_prototypeCell) {
        _prototypeCell = [self.tableView dequeueReusableCellWithIdentifier:COMMENT_CELL_IDENTIFIER];
    }
    return _prototypeCell;
}

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
        
    // table header
    [self setupImage];
    self.thumbnail = [User thumbnailOfUser:self.post.poster forSize:self.thumbnailView.frame.size];
    self.userLabel.text = self.post.poster.username;
    if ([self.post.usersWhoRated containsObject:[User currentUser]] ||
        [self.post.club.objective.expiresAt timeIntervalSinceNow] < 0) {
        self.rateButton.enabled = NO;
        self.ratingSlider.enabled = NO;
    }
    if ([self.post.numberOfRatings intValue] > 0) {
        self.averageRatingLabel.text = [NSString stringWithFormat:@"Rating: %.1f", [self.post.averageRating floatValue]];
        self.ratingSlider.value = [self.post.averageRating floatValue];
        self.sliderRatingLabel.text = [NSString stringWithFormat:@"%.1f", [self.post.averageRating floatValue]];
    } else {
        self.averageRatingLabel.text = @"No Ratings";
        self.ratingSlider.value = (self.ratingSlider.minimumValue + self.ratingSlider.maximumValue) / 2.0f;
        self.sliderRatingLabel.text = @"--";
    }
    
    UIView *headerView = self.tableView.tableHeaderView;
    CGSize preferredSize = [headerView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    headerView.frame = CGRectMake(headerView.frame.origin.x,
                                               headerView.frame.origin.y,
                                               preferredSize.width, preferredSize.height);
    self.tableView.tableHeaderView = headerView;

    self.commentTextView.delegate = self;
    self.commentTextView.layer.cornerRadius = 5.0f;
    self.commentTextView.layer.borderWidth = 1.0f;
    self.commentTextView.layer.borderColor = [[UIColor grayColor] CGColor];
    [self setPlaceholderText];
    
    self.fastPan.delegate = self;
}

#pragma mark - Core Data

- (NSFetchRequest *)createFetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Comment"];
    request.predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF", self.post.comments];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt"
                                                              ascending:YES
                                                               selector:@selector(compare:)]];
    return request;
}

#pragma mark - Image

- (void)setupImage {
    self.photoView.contentMode = UIViewContentModeCenter | UIViewContentModeTop;
    if (!self.photoView.image) {
        UIImage *photo = nil;
        if (self.post.photoData) {
            photo = [UIImage imageWithData:self.post.photoData];
        } else {
            photo = [UIImage imageNamed:self.post.photoURL];
        }
        self.photoView.image = [ImageHelper resizedImage:photo
                                      constrainedToWidth:self.photoView.frame.size.width];
    }
}

#pragma mark - Actions

- (IBAction)ratingSliderMoved:(UISlider *)sender {
    self.sliderRatingLabel.text = [NSString stringWithFormat:@"%.1f", sender.value];
}

- (IBAction)rateButtonPressed {
    self.rateButton.enabled = NO;
    self.ratingSlider.enabled = NO;
    [[User currentUser] addRatedPostsObject:self.post];
    int numRatings = [self.post.numberOfRatings intValue];
    double averageRating = [self.post.averageRating doubleValue];
    double totalRating = numRatings * averageRating;
    double newAverageRating = (totalRating + self.ratingSlider.value) / (double)(++numRatings);
    self.post.numberOfRatings = @(numRatings);
    self.post.averageRating = @(newAverageRating);
    self.averageRatingLabel.text = [NSString stringWithFormat:@"Rating: %.1f", newAverageRating];
}

- (IBAction)sendCommentButton {
    if (self.sendButton.isEnabled) {
        [self.commentTextView resignFirstResponder];
        Comment *comment = [Comment createComment:self.commentTextView.text
                                        inContext:self.post.managedObjectContext];
        comment.post = self.post;
        comment.commenter = [User currentUser];
        self.commentTextView.text = @"";
        [self setPlaceholderText];
        [self performSelector:@selector(scrollToObject:) withObject:comment afterDelay:0.5];
    }
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self.commentTextView resignFirstResponder];
}

- (IBAction)fastPan:(UIPanGestureRecognizer *)sender {
    CGPoint velocity = [sender velocityInView:self.view];
    if (velocity.y > 1300) {
        [self.commentTextView resignFirstResponder];
    }
}

#pragma mark - Table View Data Source

- (void)configureCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[CommentCell class]]) {
        CommentCell *commentCell = (CommentCell *)cell;
        Comment *comment = [self.fetchedResultsController objectAtIndexPath:indexPath];
        commentCell.thumbnail = [User thumbnailOfUser:comment.commenter forSize:commentCell.thumbnailView.frame.size];
        commentCell.commentLabel.text = [NSString stringWithFormat:@"%@: %@", comment.commenter.username, comment.text];
        NSMutableAttributedString *attributedText = [commentCell.commentLabel.attributedText mutableCopy];
        NSRange boldRange = NSMakeRange(0, [comment.commenter.username length] + 1);
        [attributedText setAttributes:@{ NSFontAttributeName : [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline] }
                                range:boldRange];
        commentCell.commentLabel.attributedText = attributedText;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:COMMENT_CELL_IDENTIFIER
                                                            forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    // for device rotation:
    self.prototypeCell.bounds = CGRectMake(0, 0, CGRectGetWidth(self.tableView.bounds),
                                           CGRectGetHeight(self.prototypeCell.bounds));
    [self.prototypeCell layoutIfNeeded];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action
forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return action == @selector(copy:) ? YES : NO;
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action
forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    Comment *comment = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [[UIPasteboard generalPasteboard] setValue:comment.text forPasteboardType:(NSString *)kUTTypeText];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([gestureRecognizer isEqual:self.fastPan]) {
        return [touch.view isEqual:self.ratingSlider] ? NO : YES;
    }
    return YES;
}

#pragma mark - UITextViewDelegate

- (void)setPlaceholderText {
    self.sendButton.enabled = [self.commentTextView.text length] ? YES : NO;
    if (![self.commentTextView.text length]) {
        self.commentTextView.text = placeholderText;
        self.commentTextView.textColor = [UIColor grayColor];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    self.sendButton.enabled = [textView.text length] ? YES : NO;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.kbAvoidingScrollView.panGestureRecognizer.enabled = NO;
    self.fastPan.enabled = YES;
    if ([self.commentTextView.text isEqualToString:placeholderText]) {
        self.commentTextView.text = @"";
        self.commentTextView.textColor = [UIColor blackColor];
    }
    self.sendButton.enabled = [textView.text length] ? YES : NO;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.kbAvoidingScrollView.panGestureRecognizer.enabled = YES;
    self.fastPan.enabled = NO;
    [self setPlaceholderText];
}

@end
