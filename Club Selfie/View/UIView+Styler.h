//
//  UIView+Styler.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/29/14.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Styler)

// colors
- (void)mediumGreenBackground;
- (void)goldBackground;
- (void)oceanBlueBackground;
- (void)tropicalBlueBackground;
- (void)bambooGradientBackground;

// corners
- (void)roundCorners;

// edges
- (void)borderWithColor:(UIColor *)color;
- (void)noBorder;

@end
