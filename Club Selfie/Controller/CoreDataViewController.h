//
//  CoreDataTVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import <CoreData/CoreData.h>

@interface CoreDataViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate>

// The controller (this class fetches nothing if this is not set).
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

// list of all table views within this view controller
@property (strong, nonatomic) NSMutableArray *tableViews;
// if there is only 1 table view as a subview, it will have this VC as delegate and datasource
@property (weak, nonatomic) UITableView *tableView;

// scrolls the table view to the passed Core Data managed object
- (void)scrollToObject:(id)obj;

// scrolls to top of table view
- (void)scrollToTop;

// This will automatically be called if you change the fetchedResultsController @property.
- (void)performFetch;

// Set to YES to get some debugging output in the console.
@property BOOL debug;

@end
