//
//  SlideOutTabBarController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/15/14.
//
//

#import "SlideOutTabBarController.h"

@interface SlideOutTabBarController ()

@end

@implementation SlideOutTabBarController

- (IBAction)unwindToTabBarController:(UIStoryboardSegue *)segue { }

- (void)viewDidLoad {
    [super viewDidLoad];
    // don't go under the top view
    self.edgesForExtendedLayout = UIRectEdgeTop | UIRectEdgeBottom | UIRectEdgeLeft;
}

@end
