//
//  ClubsVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/22/14.
//
//

#import "AddUsersToClubVC.h"
#import "CustomButton.h"
#import "ColorHelper.h"
#import "User+Create.h"
#import "ClubsVC.h"
#import "PostsVC.h"
#import "Comment.h"
#import "Club.h"
#import "Post.h"

@interface ClubsVC () <UIAlertViewDelegate>

@property (strong, nonatomic) Club *selectedClub;

@end

@implementation ClubsVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [ColorHelper oceanBlue];
    self.title = @"Clubs";
}

#pragma mark - Properties

- (void)setContext:(NSManagedObjectContext *)context {
    _context = context;
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self createFetchRequest]
                                                                        managedObjectContext:context
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

#pragma mark - Core Data

- (NSFetchRequest *)createFetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Club"];
    request.predicate = [NSPredicate predicateWithFormat:@"members.unique CONTAINS %@", [User currentUser].unique];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"title"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    return request;
}

#pragma mark - Actions

- (IBAction)editButtonPressed:(UIBarButtonItem *)sender {
    UIBarButtonSystemItem item = self.tableView.editing ? UIBarButtonSystemItemEdit : UIBarButtonSystemItemDone;
    [self.tableView setEditing:!self.tableView.editing animated:YES];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                           target:self
                                                           action:@selector(editButtonPressed:)];
}

- (IBAction)addButtonPressed:(UIBarButtonItem *)sender {
    if (self.context) {
        UINavigationController *addClubNav = [self.storyboard instantiateViewControllerWithIdentifier:ADD_CLUB_NAVIGATION_VC_STORYBOARD_ID];
        AddUsersToClubVC *autcvc = (AddUsersToClubVC *)addClubNav.viewControllers[0];
        autcvc.context = self.context;
        autcvc.editingExistingClub = NO;
        [self presentViewController:addClubNav animated:YES completion:NULL];
    }
}

#pragma mark - Table View Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CLUB_CELL_IDENTIFIER
                                                            forIndexPath:indexPath];
    
    Club *club = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = club.title;
    if ([club.administrators containsObject:[User currentUser]]) {
        [self addAdminKeyToCell:cell];
    } else {
        cell.accessoryView = nil;
    }
    
    return cell;
}

- (void)addAdminKeyToCell:(UITableViewCell *)cell {
    CGFloat buttonHeight = cell.bounds.size.height * 0.71;
    CGSize buttonSize = CGSizeMake(buttonHeight * 1.50, buttonHeight);
    CustomButton *button = [[CustomButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
    button.chosen = NO;
    button.customType = CustomButtonTypeAdmin;
    for (UIGestureRecognizer *gr in button.gestureRecognizers) {
        gr.enabled = NO;
    }
    cell.accessoryView = button;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedClub = [self.fetchedResultsController objectAtIndexPath:indexPath];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Leave Club"
                                       message:[NSString stringWithFormat:@"Are you sure you want to leave %@?", self.selectedClub.title]
                                      delegate:nil
                             cancelButtonTitle:@"No"
                             otherButtonTitles:@"Yes", nil];
    alert.delegate = self;
    [alert show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != alertView.cancelButtonIndex && self.selectedClub != nil) {
        NSString *lastViewedClubID = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_VIEWED_CLUB_KEY];
        if ([lastViewedClubID isEqualToString:self.selectedClub.unique]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CurrentClubDeletedNotification
                                                                object:self];
        }
        
        for (Post *post in self.selectedClub.posts) {
            for (Comment *comment in post.comments) {
                [self.context deleteObject:comment];
            }
            [self.context deleteObject:post];
        }
        [self.context deleteObject:self.selectedClub];
        self.selectedClub = nil;
    }
}

- (void)alertViewCancel:(UIAlertView *)alertView {
    self.selectedClub = nil;
}

#pragma mark - Navigation

- (void)prepareViewController:(id)vc
                     forSegue:(NSString *)segueIdentifier
                fromIndexPath:(NSIndexPath *)indexPath {
    UIViewController *root = ((UINavigationController *)vc).viewControllers[0];
    if ([root isKindOfClass:[PostsVC class]]) {
        if (![segueIdentifier length]) {
            Club *club = [self.fetchedResultsController objectAtIndexPath:indexPath];
            PostsVC *postsVC = (PostsVC *)root;
            postsVC.club = club;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = nil;
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        indexPath = [self.tableView indexPathForCell:sender];
    }
    [self prepareViewController:segue.destinationViewController
                       forSegue:segue.identifier
                  fromIndexPath:indexPath];
}

@end
