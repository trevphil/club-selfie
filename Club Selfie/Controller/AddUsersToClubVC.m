//
//  AddClubVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/5/14.
//
//

#import "AddFriendToClubCell.h"
#import "AddUsersToClubVC.h"
#import "NewClubInfoVC.h"
#import "ColorHelper.h"

@interface AddUsersToClubVC () <UITextFieldDelegate, UISearchDisplayDelegate>

@property (weak, nonatomic) IBOutlet UITextField *clubNameTextField;
@property (strong, nonatomic) NSMutableArray *newMembers;
@property (strong, nonatomic) NSMutableArray *newAdmins;

@end

@implementation AddUsersToClubVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.editingExistingClub) {
        self.navigationController.navigationBar.barTintColor = [ColorHelper mediumGreen];
        [self.clubNameTextField removeFromSuperview];
        self.clubNameTextField = nil;
        self.navigationItem.titleView = nil;
        self.title = @"Add Members";
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                                               target:self action:@selector(unwind)];
        [self.newMembers addObjectsFromArray:[self.club.members allObjects]];
        [self.newAdmins addObjectsFromArray:[self.club.administrators allObjects]];
    } else {
        self.navigationController.navigationBar.barTintColor = [ColorHelper oceanBlue];
        self.title = @"Back";
    }
}

- (void)unwind {
    NSMutableSet *membersToRemove = [[NSMutableSet alloc] init];
    for (User *user in self.fetchedResultsController.fetchedObjects) {
        if ([self.club.members containsObject:user] && ![self.newMembers containsObject:user]) {
            [membersToRemove addObject:user];
        }
    }
    [self.club removeMembers:membersToRemove];
    [self.club addMembers:[NSSet setWithArray:self.newMembers]];
    [self.club addAdministrators:[NSSet setWithArray:self.newAdmins]];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // setup title text field
    if (!self.editingExistingClub) {
        [self updateTextFieldFrame];
        self.clubNameTextField.placeholder = @"Club Name";
        self.clubNameTextField.delegate = self;
    }
}

- (void)updateTextFieldFrame {
    CGRect newTextFieldFrame = self.clubNameTextField.frame;
    newTextFieldFrame.size.height = self.navigationController.navigationBar.frame.size.height;
    self.clubNameTextField.frame = newTextFieldFrame;
    
    CGFloat fontSize = self.clubNameTextField.frame.size.height * 0.65;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont fontWithName:@"OleoScript-Regular"
                                                                        size:fontSize],
                                  NSForegroundColorAttributeName : [UIColor whiteColor] };
    self.clubNameTextField.defaultTextAttributes = attributes;
    self.clubNameTextField.textAlignment = NSTextAlignmentCenter;
}

#pragma mark - Rotation

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    if (!self.editingExistingClub) {
        [UIView animateWithDuration:0.6 animations:^{
            [self updateTextFieldFrame];
        }];
    }
}

#pragma mark - Properties

- (NSMutableArray *)newMembers {
    if (!_newMembers) {
        _newMembers = [[NSMutableArray alloc] initWithObjects:[User currentUser], nil];
    }
    return _newMembers;
}

- (NSMutableArray *)newAdmins {
    if (!_newAdmins) {
        _newAdmins = [[NSMutableArray alloc] initWithObjects:[User currentUser], nil];
    }
    return _newAdmins;
}

#pragma mark - Actions

- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)addButton:(UIButton *)sender {
    AddFriendToClubCell *cell = (AddFriendToClubCell *)[self superviewOfView:sender
                                                                    forClass:[AddFriendToClubCell class]
                                                            currentIteration:0];
    UITableView *tableView = self.searchDisplayController.isActive ?
    self.searchDisplayController.searchResultsTableView : self.tableView;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    User *user = self.searchDisplayController.isActive ?
    self.searchResults[indexPath.row] : [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([self.newMembers containsObject:user]) {
        [self.newMembers removeObject:user];
        [self.newAdmins removeObject:user];
    } else {
        [self.newMembers addObject:user];
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (IBAction)makeAdmin:(UIButton *)sender {
    AddFriendToClubCell *cell = (AddFriendToClubCell *)[self superviewOfView:sender
                                                                    forClass:[AddFriendToClubCell class]
                                                            currentIteration:0];
    UITableView *tableView = self.searchDisplayController.isActive ?
    self.searchDisplayController.searchResultsTableView : self.tableView;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    User *user = self.searchDisplayController.isActive ?
    self.searchResults[indexPath.row] : [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([self.newAdmins containsObject:user]) {
        [self.newAdmins removeObject:user];
    } else {
        [self.newMembers addObject:user];
        [self.newAdmins addObject:user];
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewCell

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user {
    AddFriendToClubCell *aftcc = (AddFriendToClubCell *)cell;
    aftcc.usernameLabel.text = user.username;
    aftcc.thumbnail = [User thumbnailOfUser:user forSize:aftcc.thumbnailView.frame.size];
    
    if ([self.newMembers containsObject:user]) {
        aftcc.addedButton.chosen = YES;
    } else {
        aftcc.addedButton.chosen = NO;
    }
    
    if ([self.newAdmins containsObject:user]) {
        aftcc.adminButton.chosen = YES;
    } else {
        aftcc.adminButton.chosen = NO;
    }
    
    if (self.editingExistingClub) {
        BOOL userIsAdmin = [self.club.administrators containsObject:user];
        aftcc.addedButton.enabled = !userIsAdmin;
        [aftcc.addedButton setTintColor:[UIColor grayColor]];
        aftcc.adminButton.enabled = !userIsAdmin;
    }
}

- (UITableViewCell *)createSearchCell {
    return [self.tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
    return cell.frame.size.height;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayController:(UISearchDisplayController *)controller
 willHideSearchResultsTableView:(UITableView *)tableView {
    [self.tableView reloadData];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[NewClubInfoVC class]]) {
        NewClubInfoVC *ncivc = (NewClubInfoVC *)segue.destinationViewController;
        ncivc.clubName = self.clubNameTextField.text;
        ncivc.clubMembers = [NSSet setWithArray:self.newMembers];
        ncivc.clubAdmins = [NSSet setWithArray:self.newAdmins];
    }
}

@end
