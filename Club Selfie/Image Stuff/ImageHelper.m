//
//  ImageHelper.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/22/14.
//
//

#import "ImageHelper.h"
#import <ImageIO/ImageIO.h>

@implementation ImageHelper

#pragma mark - Face Detection!

+ (UIImage *)faceCenteredSquareImage:(UIImage *)image {
    CIImage *outputImage;
    
    // setup CIImage from UIImage
    CIImage *inputImage = [CIImage imageWithCGImage:[[self class] correctOrientation:image].CGImage];
    CGRect inputImageRect = [inputImage extent];
    CGFloat sideLength = inputImageRect.size.width < inputImageRect.size.height ?
    inputImageRect.size.width : inputImageRect.size.height;
    
    // setup face detector
    CIContext *context = [CIContext contextWithOptions:nil];
    NSDictionary *opts = @{ CIDetectorAccuracy : CIDetectorAccuracyLow };
    CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeFace context:context options:opts];
    NSArray *faces = [detector featuresInImage:inputImage options:nil]; // array of CIFeatures
    if (![faces count]) {
        outputImage = [inputImage imageByCroppingToRect:CGRectMake(0, 0, sideLength, sideLength)];
        //return [UIImage imageWithCIImage:outputImage];
        return [[self class] imageFromCIImage:outputImage context:context];
    }
    
    // find face which is closest to the CIImage's center
    CGPoint center = CGPointMake(CGRectGetMidX(inputImageRect), CGRectGetMidY(inputImageRect));
    CGPoint closestPointToCenter = CGPointZero;
    CGFloat minDistance = CGFLOAT_MAX;
    for (CIFeature *face in faces) {
        CGFloat faceCenterX = face.bounds.origin.x + face.bounds.size.width / 2.0;
        CGFloat faceCenterY = face.bounds.origin.y + face.bounds.size.height / 2.0;
        CGFloat distance = hypotf(faceCenterX - center.x, faceCenterY - center.y);
        if (distance < minDistance) {
            minDistance = distance;
            closestPointToCenter = CGPointMake(faceCenterX, faceCenterY);
        }
    }
    
    // find the cropping square (but keep it within the CIImage)
    CGRect cropRect = CGRectMake(closestPointToCenter.x - sideLength / 2.0,
                                 closestPointToCenter.y - sideLength / 2.0,
                                 sideLength, sideLength);
    if (cropRect.origin.x < 0) {
        cropRect.origin.x = 0;
    } else if (cropRect.origin.x + sideLength > inputImageRect.size.width) {
        cropRect.origin.x = inputImageRect.size.width - sideLength;
    }
    if (cropRect.origin.y < 0) {
        cropRect.origin.y = 0;
    } else if (cropRect.origin.y + sideLength > inputImageRect.size.height) {
        cropRect.origin.y = inputImageRect.size.height - sideLength;
    }
    
    // crop and convert to UIImage
    outputImage = [inputImage imageByCroppingToRect:cropRect];
    //return [UIImage imageWithCIImage:outputImage];
    return [[self class] imageFromCIImage:outputImage context:context];
}

+ (UIImage *)imageFromCIImage:(CIImage *)image context:(CIContext *)cxt {
    CGImageRef ref = [cxt createCGImage:image fromRect:[image extent]];
    UIImage *newImage = [UIImage imageWithCGImage:ref];
    CGImageRelease(ref);
    return newImage;
}

#pragma mark - Orientation

// for some reason, photos in portrait mode are rotated 90 degrees when converted from UIImage -> CIImage
+ (UIImage *)correctOrientation:(UIImage *)image {
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // we need to calculate the proper transformation to make the image upright
    // 2 steps: rotate if left/right/down, and then flip if mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // now draw the underlying CGImage into a new context, applying the transform calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.height, image.size.width), image.CGImage);
            break;
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
            break;
    }
    
    // and now we just create a new UIImage from the drawing context
    CGImageRef ref = CGBitmapContextCreateImage(ctx);
    UIImage *corrected = [UIImage imageWithCGImage:ref];
    CGContextRelease(ctx);
    CGImageRelease(ref);
    return corrected;
}

#pragma mark - Resizing

+ (UIImage *)resizedImage:(UIImage *)image constrainedToWidth:(CGFloat)width {
    if (image.size.width > width) {
        CGFloat newHeight = image.size.height * (width / image.size.width);
        return [self resizedImage:image scaledToSize:CGSizeMake(width, newHeight)];
    } else {
        return image;
    }
}

+ (UIImage *)resizedImage:(UIImage *)image constrainedToHeight:(CGFloat)height {
    if (image.size.height > height) {
        CGFloat newWidth = image.size.width * (height / image.size.height);
        return [self resizedImage:image scaledToSize:CGSizeMake(newWidth, height)];
    } else {
        return image;
    }
}

+ (UIImage *)resizedImage:(UIImage *)image scaledToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

@end
