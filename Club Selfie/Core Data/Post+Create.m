//
//  Post+Create.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Post+Create.h"

@implementation Post (Create)

+ (Post *)createPostInContext:(NSManagedObjectContext *)context {
    Post *post = [NSEntityDescription insertNewObjectForEntityForName:@"Post"
                                                     inManagedObjectContext:context];
    post.title = @"So smart!";
    post.createdAt = [NSDate date];
    if (arc4random() % 2) {
        post.photoURL = @"smallPhoto.png";
        post.aspectRatio = @(128.0 / 120.0);
    } else {
        post.photoURL = @"photo.JPG";
        post.aspectRatio = @(960.0 / 1280.0);
    }
    return post;
}

+ (Post *)createPostInContext:(NSManagedObjectContext *)context withData:(NSData *)data {
    Post *post = [[self class] createPostInContext:context];
    post.photoData = data;
    post.photoURL = nil;
    post.title = nil;
    post.aspectRatio = nil;
    post.averageRating = nil;
    post.numberOfRatings = 0;
    return post;
}

@end
