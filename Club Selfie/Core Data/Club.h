//
//  Club.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Objective, Post, User;

@interface Club : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * lastActiveAt;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) NSSet *administrators;
@property (nonatomic, retain) NSSet *members;
@property (nonatomic, retain) Objective *objective;
@property (nonatomic, retain) NSSet *posts;
@end

@interface Club (CoreDataGeneratedAccessors)

- (void)addAdministratorsObject:(User *)value;
- (void)removeAdministratorsObject:(User *)value;
- (void)addAdministrators:(NSSet *)values;
- (void)removeAdministrators:(NSSet *)values;

- (void)addMembersObject:(User *)value;
- (void)removeMembersObject:(User *)value;
- (void)addMembers:(NSSet *)values;
- (void)removeMembers:(NSSet *)values;

- (void)addPostsObject:(Post *)value;
- (void)removePostsObject:(Post *)value;
- (void)addPosts:(NSSet *)values;
- (void)removePosts:(NSSet *)values;

@end
