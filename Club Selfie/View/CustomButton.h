//
//  AddFriendButton.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

@interface CustomButton : UIButton

typedef NS_ENUM(NSInteger, CustomButtonType) {
    CustomButtonTypeFriend,
    CustomButtonTypeAdded,
    CustomButtonTypeAdmin
};

@property (nonatomic, getter = isChosen) BOOL chosen;
@property (nonatomic) CustomButtonType customType;

@end
