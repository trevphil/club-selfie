//
//  TutorialRootVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/15/14.
//
//

#import "TutorialRootVC.h"
#import "TutorialContentVC.h"

@interface TutorialRootVC ()

@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@end

@implementation TutorialRootVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [self setPageViewControllerFrame];

    [self setupPageControl];

    [self.pageViewController setViewControllers:@[[self viewControllerAtIndex:0]]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO completion:NULL];
    
    CGSize size = self.closeButton.frame.size;
    CALayer *layer = self.closeButton.layer;
    layer.shadowOpacity = 0.75f;
    layer.shadowOffset = CGSizeZero;
    layer.shadowPath = [[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, size.width, size.height)] CGPath];
    self.closeButton.layer.cornerRadius = MIN(self.closeButton.frame.size.width,
                                              self.closeButton.frame.size.height) / 2.0 * 0.99;
    [self.view bringSubviewToFront:self.closeButton];
}

- (void)setPageViewControllerFrame {
    CGRect frame = self.view.frame;
    CGFloat heightOffset = MIN([UIApplication sharedApplication].statusBarFrame.size.width,
                               [UIApplication sharedApplication].statusBarFrame.size.height);
    frame.size.height -= heightOffset;
    frame.origin.y += heightOffset;
    self.pageViewController.view.frame = frame;
}

- (void)setupPageControl {
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
}

#pragma mark - Properties

- (NSArray *)pageImages {
    return @[@"MainScreen.png",
             @"SamplePost.png",
             @"Info.png",
             @"Clubs.png",
             @"CreateClub.png",
             @"Friends.png"];
}

- (UIPageViewController *)pageViewController {
    if (!_pageViewController) {
        _pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:TUTORIAL_PAGE_VC_STORYBOARD_ID];
        _pageViewController.dataSource = self;
    }
    return _pageViewController;
}

#pragma mark - Actions

- (IBAction)closeButtonPressed:(UIButton *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index {
    if ([self.pageImages count] == 0 || index >= [self.pageImages count]) {
        return nil;
    }
    
    TutorialContentVC *tcvc = [self.storyboard instantiateViewControllerWithIdentifier:TUTORIAL_CONTENT_STORYBOARD_ID];
    tcvc.imageFile = self.pageImages[index];
    tcvc.pageIndex = index;
    return tcvc;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
      viewControllerBeforeViewController:(UIViewController *)viewController {
    NSUInteger index = ((TutorialContentVC *)viewController).pageIndex;
    return (index == 0 || index == NSNotFound) ? nil : [self viewControllerAtIndex:(--index)];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController
       viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = ((TutorialContentVC *)viewController).pageIndex;
    return (index == NSNotFound || ++index == [self.pageImages count]) ? nil : [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return [self.pageImages count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // i.e. start on the first page
    return 0;
}

@end
