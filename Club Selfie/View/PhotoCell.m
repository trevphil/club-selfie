//
//  PhotoCell.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/23/14.
//
//

#import "PhotoCell.h"

@interface PhotoCell ()
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;

@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) UIImage *photo;

@end

@implementation PhotoCell

#pragma mark - Properties

- (UIImage *)thumbnail {
    return self.thumbnailView.image;
}

- (void)setThumbnail:(UIImage *)thumbnail {
    self.thumbnailView.image = thumbnail;
}

- (UIImage *)photo {
    return self.photoView.image;
}

- (void)setPhoto:(UIImage *)photo {
    self.photoView.image = photo;
}

@end
