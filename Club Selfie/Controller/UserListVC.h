//
//  UserListVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/5/14.
//
//

#import "CoreDataViewController.h"
#import "User+Create.h"

@interface UserListVC : CoreDataViewController

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSArray *searchResults;

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user;
- (UITableViewCell *)createSearchCell;
- (UIView *)superviewOfView:(UIView *)view
                   forClass:(__unsafe_unretained Class)aClass
           currentIteration:(NSUInteger)iter;

@end
