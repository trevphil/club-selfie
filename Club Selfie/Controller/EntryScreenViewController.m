//
//  EntryScreenViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "EntryScreenViewController.h"
#import "SlidingViewController.h"
#import "SignUpViewController.h"
#import "LoginViewController.h"

@interface EntryScreenViewController ()

@end

@implementation EntryScreenViewController

- (IBAction)cancelBarButton:(UIBarButtonItem *)sender {
    SlidingViewController *svc = (SlidingViewController *)self.presentingViewController;
    [svc dismissViewControllerAnimated:YES completion:^{
        [svc showTutorialIfNeeded];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[SignUpViewController class]]) {
        SignUpViewController *signUpVC = (SignUpViewController *)segue.destinationViewController;
        if ([segue.identifier isEqualToString:SIGN_UP_SEGUE_IDENTIFIER]) {
            // prepare signUpVC (you can change these settings)
            signUpVC.context = self.context;
            signUpVC.minUsernameLength = 4;
            signUpVC.minPasswordLength = 6;
            signUpVC.passwordRequiresNumber = NO;
            signUpVC.passwordRequiresCapitalLetter = NO;
        }
    } else if ([segue.destinationViewController isKindOfClass:[LoginViewController class]]) {
        LoginViewController *loginVC = (LoginViewController *)segue.destinationViewController;
        if ([segue.identifier isEqualToString:LOGIN_SEGUE_IDENTIFIER]) {
            // prepare loginVC (not required to specify login type as email/username)
            //loginVC.loggingInWithEmail = NO;
            loginVC.context = self.context;
        }
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
