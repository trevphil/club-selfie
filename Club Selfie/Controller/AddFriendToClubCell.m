//
//  AddFriendToClubCell.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/6/14.
//
//

#import "AddFriendToClubCell.h"

@interface AddFriendToClubCell ()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet CustomButton *addedButton;
@property (weak, nonatomic) IBOutlet CustomButton *adminButton;

@end

@implementation AddFriendToClubCell

- (void)awakeFromNib {
    self.addedButton.chosen = NO;
    self.addedButton.customType = CustomButtonTypeAdded;
    
    self.adminButton.chosen = NO;
    self.adminButton.customType = CustomButtonTypeAdmin;
}

- (UIImage *)thumbnail {
    return self.thumbnailView.image;
}

- (void)setThumbnail:(UIImage *)thumbnail {
    self.thumbnailView.image = thumbnail;
}

@end
