//
//  Club.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import "Club.h"
#import "Objective.h"
#import "Post.h"
#import "User.h"


@implementation Club

@dynamic createdAt;
@dynamic lastActiveAt;
@dynamic title;
@dynamic unique;
@dynamic administrators;
@dynamic members;
@dynamic objective;
@dynamic posts;

@end
