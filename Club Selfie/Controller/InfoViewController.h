//
//  InfoViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/10/14.
//
//

#import "CoreDataViewController.h"
#import "User+Create.h"
#import "Objective.h"
#import "Club.h"

@interface InfoViewController : CoreDataViewController

@property (strong, nonatomic) Club *club;
@property (strong, nonatomic) Objective *objective;
@property (weak, nonatomic, readonly) UIDatePicker *datePicker;
@property (nonatomic) CGFloat datePickerOriginalHeight;
@property (nonatomic, getter = isAdmin) BOOL admin;
@property (strong, nonatomic) UITapGestureRecognizer *dismissKeyboardGesture;
@property (weak, nonatomic, readonly) UITextView *objectiveTextView;

- (int)timeLeft;
- (void)setupTimer;
- (void)updateCountdown;
- (void)updateFetchedResultsController;
- (void)updateHeaderViewHeight;

@end
