//
//  MainScreenViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/19/14.
//
//

#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>
#import "UIViewController+AddPhoto.h"
#import "PhotoButtonLayerView.h"
#import "ExistingClubInfoVC.h"
#import "SelfiePostVC.h"
#import "PostPhotoVC.h"
#import "ColorHelper.h"
#import "User+Create.h"
#import "PhotoCell.h"
#import "PostsVC.h"
#import "Post.h"

@interface PostsVC ()

@property (strong, nonatomic) IBOutlet PhotoButtonLayerView *photoButtonLayerView;
@property (strong, nonatomic) Post *winningPost;
@property (weak, nonatomic) IBOutlet UILabel *winnerLabel;
@property (nonatomic) BOOL tie;

@end

@implementation PostsVC

- (IBAction)swipe:(UISwipeGestureRecognizer *)sender {
    ECSlidingViewController *slidingViewController = [self slidingViewController];
    if (slidingViewController) {
        [slidingViewController anchorTopViewToRightAnimated:YES];
    }
}

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createShadow];
    [self checkForClub];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserverForName:UserInfoChangedNotification
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *note) {
                                                      [self.tableView reloadData];
                                                  }];
    [self checkForWinner];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createShadow {
    CALayer *layer = self.parentViewController.view.layer;
    layer.shadowOpacity = 0.75f;
    CGSize extended = layer.bounds.size;
    if (extended.width > extended.height) { // extend shadow if we're in landscape
        extended.height = extended.width;
    }
    layer.shadowPath = [[UIBezierPath bezierPathWithRect:CGRectMake(0, 0, extended.width, extended.height)] CGPath];
}

#pragma mark - Properties

- (void)setClub:(Club *)club {
    _club = club;
    [[NSUserDefaults standardUserDefaults] setObject:(club ? club.unique : @"") forKey:LAST_VIEWED_CLUB_KEY];
    if ([self checkForClub]) {
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self createFetchRequest]
                                                                            managedObjectContext:self.club.managedObjectContext
                                                                              sectionNameKeyPath:nil
                                                                                       cacheName:nil];
    }
    [self checkForWinner];
}

- (void)setWinningPost:(Post *)winningPost {
    _winningPost = winningPost;
    [self setupTableHeader];
}

- (BOOL)checkForClub {
    if (self.club) {
        self.title = self.club.title;
        self.photoButtonLayerView.hidden = NO;
        return YES;
    } else {
        self.title = @"No Club Chosen";
        self.photoButtonLayerView.hidden = YES;
        return NO;
    }
}

#pragma mark - Winner

- (void)checkForWinner {
    Post *oldWinner = self.winningPost;
    
    if ([self.club.objective.expiresAt timeIntervalSinceNow] <= 0) {
        NSArray *postsByRating = [[self.club.posts allObjects] sortedArrayWithOptions:NSSortConcurrent
          usingComparator:^NSComparisonResult(Post *post1, Post *post2) {
              double rating1 = post1.averageRating ? [post1.averageRating doubleValue] : 0;
              double rating2 = post2.averageRating ? [post2.averageRating doubleValue] : 0;
              double diff = rating1 - rating2;
              if (diff > 0) {
                  return NSOrderedAscending;
              } else if (diff < 0) {
                  return NSOrderedDescending;
              } else {
                  return NSOrderedSame;
              }
          }];
        self.winningPost = [postsByRating count] ? postsByRating[0] : nil;
        if ([postsByRating count] > 1) {
            Post *post1 = postsByRating[0];
            Post *post2 = postsByRating[1];
            if (post1.averageRating && post2.averageRating) {
                self.tie = [post1.averageRating isEqualToNumber:post2.averageRating];
            }
        }
    } else {
        self.winningPost = nil;
    }
    
    NSIndexPath *indexPath = nil;
    if (oldWinner && !self.winningPost) {
        indexPath = [self.fetchedResultsController indexPathForObject:oldWinner];
    } else if (self.winningPost && !oldWinner) {
        indexPath = [self.fetchedResultsController indexPathForObject:self.winningPost];
    }
    if (indexPath) {
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    self.photoButtonLayerView.hidden = [self.club.objective.expiresAt timeIntervalSinceNow] <= 0 ? YES : NO;
}

- (void)setupTableHeader {
    UIView *headerView = self.tableView.tableHeaderView;
    CGRect headerFrame = headerView.frame;
    if (self.winningPost) {
        if (self.tie) {
            self.winnerLabel.text = @"Tie!";
        } else {
            self.winnerLabel.text = [NSString stringWithFormat:@"Winner: %@", self.winningPost.poster.username];
        }
        [self.winnerLabel sizeToFit];
        headerFrame.size.height = self.winnerLabel.frame.size.height * 2.0;
        headerView.backgroundColor = [ColorHelper gold];
    } else {
        self.winnerLabel.text = @"";
        headerFrame.size.height = 0;
        headerView.backgroundColor = nil;
    }
    headerView.frame = headerFrame;
    self.tableView.tableHeaderView = headerView;
}

#pragma mark - Camera and Photos

- (IBAction)photoButtonTap:(UIButton *)sender {
    [self showActionSheet];
}

- (void)pickedPhoto:(UIImage *)photo {
    UINavigationController *nav = [self.storyboard instantiateViewControllerWithIdentifier:POST_PHOTO_NAVIGATION_VC_STORYBOARD_ID];
    PostPhotoVC *ppvc = (PostPhotoVC *)nav.viewControllers[0];
    ppvc.club = self.club;
    ppvc.photo = photo;
    [self dismissViewControllerAnimated:NO completion:^{
        [self presentViewController:nav animated:NO completion:NULL];
    }];
}

#pragma mark - Core Data

- (NSFetchRequest *)createFetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Post"];
    request.predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF", self.club.posts];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"createdAt"
                                                              ascending:NO
                                                               selector:@selector(compare:)]];
    return request;
}

#pragma mark - Table View Data Source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PhotoCell *photoCell = (PhotoCell *)[tableView dequeueReusableCellWithIdentifier:PHOTO_CELL_IDENTIFIER];
    
    Post *post = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    photoCell.userLabel.text = post.poster.username;
    photoCell.thumbnail = [User thumbnailOfUser:post.poster];
    
    int numComments = (int)[post.comments count];
    NSString *commentsText = [NSString stringWithFormat:@"%d COMMENT%@", numComments, numComments == 1 ? @"" : @"S"];
    photoCell.commentsLabel.text = numComments ? commentsText : @"NO COMMENTS";
    
    if ([post.numberOfRatings intValue] > 0) {
        photoCell.ratingLabel.text = [NSString stringWithFormat:@"%.1f", [post.averageRating floatValue]];
    } else {
        photoCell.ratingLabel.text = @"--";
    }
    
    if (post.photoData) {
        photoCell.photo = [UIImage imageWithData:post.photoData];
    } else {
        photoCell.photo = [UIImage imageNamed:post.photoURL];
    }
    
    if ([self.winningPost isEqual:post]) {
        photoCell.backgroundColor = [ColorHelper gold];
    } else {
        photoCell.backgroundColor = nil;
    }
    
    return photoCell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 376.0f; // average of the 3 possible cell type heights
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Post *post = [self.fetchedResultsController objectAtIndexPath:indexPath];
    static CGFloat cellHeightWithoutPhoto = 88.0f;
    if ([post.aspectRatio doubleValue] < 0.9) {
        return cellHeightWithoutPhoto + 373.0f;
    } else if ([post.aspectRatio doubleValue] > 1.1) {
        return cellHeightWithoutPhoto + 210.0f;
    } else {
        return cellHeightWithoutPhoto + 280.0f;
    }
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:INFO_SEGUE_IDENTIFIER] && !self.club.objective) {
        return NO;
    }
    return YES;
}

- (IBAction)unwindToPostsVC:(UIStoryboardSegue *)segue {
    if ([segue.sourceViewController isKindOfClass:[PostPhotoVC class]]) {
        PostPhotoVC *apvc = (PostPhotoVC *)segue.sourceViewController;
        if (apvc.post) {
            [self performSelector:@selector(scrollToTop) withObject:nil afterDelay:0.8];
        }
    }
}

- (void)prepareViewController:(id)vc
                     forSegue:(NSString *)segueIdentifier
                fromIndexPath:(NSIndexPath *)indexPath {
    if ([vc isKindOfClass:[SelfiePostVC class]]) {
        if (![segueIdentifier length] || [segueIdentifier isEqualToString:POST_SEGUE_IDENTIFIER]) {
            Post *post = [self.fetchedResultsController objectAtIndexPath:indexPath];
            SelfiePostVC *sptvc = (SelfiePostVC *)vc;
            sptvc.post = post;
        }
    } else if ([vc isKindOfClass:[ExistingClubInfoVC class]]) {
        if (![segueIdentifier length] || [segueIdentifier isEqualToString:INFO_SEGUE_IDENTIFIER]) {
            ExistingClubInfoVC *ecivc = (ExistingClubInfoVC *)vc;
            ecivc.club = self.club;
            ecivc.objective = self.club.objective;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = nil;
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        indexPath = [self.tableView indexPathForCell:sender];
    }
    [self prepareViewController:segue.destinationViewController
                       forSegue:segue.identifier
                  fromIndexPath:indexPath];
}

@end
