//
//  FriendCell.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/12/14.
//
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface FriendCell : UITableViewCell

@property (strong, nonatomic) UIImage *thumbnail;
@property (weak, nonatomic, readonly) UIImageView *thumbnailView;
@property (weak, nonatomic, readonly) UILabel *titleLabel;
@property (weak, nonatomic, readonly) CustomButton *addFriendButton;

@end
