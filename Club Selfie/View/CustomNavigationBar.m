//
//  GradientNavigationBar.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/1/14.
//
//

#import "CustomNavigationBar.h"
#import "ColorHelper.h"

@implementation CustomNavigationBar

- (void)setup {
    self.tintColor = [UIColor whiteColor];
    self.barTintColor = [ColorHelper mediumGreen];
    self.translucent = NO;
    
    CGFloat fontSize = self.frame.size.height * 0.65;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont fontWithName:@"OleoScript-Regular"
                                                                        size:fontSize],
                                  NSForegroundColorAttributeName : [UIColor whiteColor] };
    self.titleTextAttributes = attributes;
}

- (void)awakeFromNib {
    [self setup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}


@end
