//
//  AddFriendButton.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

#import "UIView+Drawing.h"
#import "CustomButton.h"
#import "ColorHelper.h"

@implementation CustomButton

#pragma mark - Properties

- (void)setChosen:(BOOL)chosen {
    _chosen = chosen;
    [self setNeedsDisplay];
}

- (void)setCustomType:(CustomButtonType)customType {
    _customType = customType;
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self drawButtonContent:rect inContext:context];
}

- (void)drawButtonContent:(CGRect)rect inContext:(CGContextRef)context {
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                                           cornerRadius:5.0f];
    [roundedRect addClip];
    roundedRect.lineWidth = 2.0f;
    
    switch (self.customType) {
        case CustomButtonTypeFriend:
            if (self.isChosen) {
                [self drawCheck:rect inContext:context];
            } else {
                [self drawPlus:rect inContext:context];
            }
            break;
        case CustomButtonTypeAdded:
            if (self.isChosen) {
                [self drawAdded:rect inContext:context];
            } else {
                [self drawNotAdded:rect inContext:context];
            }
            break;
        case CustomButtonTypeAdmin:
            if (self.isChosen) {
                [self drawAdmin:rect inContext:context];
            } else {
                [self drawNotAdmin:rect inContext:context];
            }
            break;
        default:
            break;
    }
    
    if (!self.isChosen) {
        [[UIColor grayColor] setStroke];
        [roundedRect stroke];
    }
}

- (void)drawPlus:(CGRect)rect inContext:(CGContextRef)context {
    [[UIColor whiteColor] setFill];
    UIRectFill(self.bounds);
    
    CGFloat plusHeight = CGRectGetHeight(rect) * 0.60;
    
    UIBezierPath *verticalLine = [UIBezierPath bezierPath];
    [verticalLine moveToPoint:CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect) - plusHeight / 2.0)];
    [verticalLine addLineToPoint:CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect) + plusHeight / 2.0)];
    
    UIBezierPath *horizontalLine = [UIBezierPath bezierPath];
    [horizontalLine moveToPoint:CGPointMake(CGRectGetMidX(rect) - plusHeight / 2.0, CGRectGetMidY(rect))];
    [horizontalLine addLineToPoint:CGPointMake(CGRectGetMidX(rect) + plusHeight / 2.0, CGRectGetMidY(rect))];
    
    verticalLine.lineWidth = 2.0f;
    verticalLine.lineCapStyle = kCGLineCapRound;
    horizontalLine.lineWidth = 2.0f;
    horizontalLine.lineCapStyle = kCGLineCapRound;
    
    [[UIColor grayColor] setStroke];
    [verticalLine stroke];
    [horizontalLine stroke];
}

- (void)drawCheck:(CGRect)rect inContext:(CGContextRef)context {
    [[ColorHelper mediumGreen] setFill];
    [[UIColor whiteColor] setStroke];
    UIRectFill(self.bounds);
    
    CGFloat checkHeight = CGRectGetHeight(rect) * 0.60;
    CGFloat checkWidth = checkHeight / 2.0;
    
    UIBezierPath *check = [UIBezierPath bezierPath];
    [check moveToPoint:CGPointMake(0, -checkHeight)];
    [check addLineToPoint:CGPointZero];
    [check addLineToPoint:CGPointMake(-checkWidth, 0)];
    check.lineWidth = 2.0f;
    check.lineCapStyle = kCGLineCapRound;
    
    [self pushContextAndTransform:^(CGContextRef context) {
        CGContextTranslateCTM(context, CGRectGetMidX(rect) * 0.90, CGRectGetHeight(rect) * 0.70);
        CGContextRotateCTM(context, M_PI_4);
    }];
    
    [check stroke];
    
    [self popContext];
}

- (void)drawAdded:(CGRect)rect inContext:(CGContextRef)context {
    [self drawCheck:rect inContext:context];
}

- (void)drawNotAdded:(CGRect)rect inContext:(CGContextRef)context {
    [self drawPlus:rect inContext:context];
}

- (void)drawAdmin:(CGRect)rect inContext:(CGContextRef)context {
    [self drawKey:rect inContext:context useGreen:YES];
}

- (void)drawNotAdmin:(CGRect)rect inContext:(CGContextRef)context {
    [self drawKey:rect inContext:context useGreen:NO];
}

- (void)drawKey:(CGRect)rect inContext:(CGContextRef)context useGreen:(BOOL)green {
    if (green) {
        [[ColorHelper mediumGreen] setFill];
        [[UIColor whiteColor] setStroke];
    } else {
        [[UIColor whiteColor] setFill];
        [[UIColor grayColor] setStroke];
    }
    UIRectFill(self.bounds);
    
    CGFloat keyholeRadius = CGRectGetWidth(rect) * 0.13;
    CGRect keyholeRect = CGRectMake(CGRectGetWidth(rect) * 0.70 - keyholeRadius,
                                    CGRectGetMidY(rect) - keyholeRadius,
                                    keyholeRadius * 2.0, keyholeRadius * 2.0);
    CGFloat keystickStartX = CGRectGetWidth(rect) - (keyholeRect.origin.x + keyholeRect.size.width);
    CGFloat keystickLength = keyholeRect.origin.x - keystickStartX;
    CGFloat prong2StartX = keystickStartX + keystickLength * 0.30;
    CGFloat prong1Height = keyholeRadius * 0.95;
    CGFloat prong2Height = keyholeRadius * 0.80;
    
    UIBezierPath *keyhole = [UIBezierPath bezierPathWithOvalInRect:keyholeRect];
    UIBezierPath *keystick = [UIBezierPath bezierPath];
    [keystick moveToPoint:CGPointMake(keystickStartX, CGRectGetMidY(rect))];
    [keystick addLineToPoint:CGPointMake(keyholeRect.origin.x, CGRectGetMidY(rect))];
    UIBezierPath *keyprong1 = [UIBezierPath bezierPath];
    [keyprong1 moveToPoint:CGPointMake(keystickStartX, CGRectGetMidY(rect))];
    [keyprong1 addLineToPoint:CGPointMake(keystickStartX, CGRectGetMidY(rect) + prong1Height)];
    UIBezierPath *keyprong2 = [UIBezierPath bezierPath];
    [keyprong2 moveToPoint:CGPointMake(prong2StartX, CGRectGetMidY(rect))];
    [keyprong2 addLineToPoint:CGPointMake(prong2StartX, CGRectGetMidY(rect) + prong2Height)];
    
    keyhole.lineWidth = 3.0f;
    keystick.lineWidth = 3.0f;
    keyprong1.lineWidth = 3.0f;
    keyprong2.lineWidth = 3.0f;
    keystick.lineCapStyle = kCGLineCapRound;
    keyprong1.lineCapStyle = kCGLineCapRound;
    keyprong2.lineCapStyle = kCGLineCapRound;
    
    [keyhole stroke];
    [keystick stroke];
    [keyprong1 stroke];
    [keyprong2 stroke];
}

#pragma mark - Initialization

- (void)awakeFromNib {
    [self commonSetup];
    [self setTitle:@"" forState:UIControlStateNormal];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
        [self setTitle:@"" forState:UIControlStateNormal];
    }
    return self;
}

@end
