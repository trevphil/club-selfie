//
//  SettingsTVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/15/14.
//
//

#import <ECSlidingViewController/UIViewController+ECSlidingViewController.h>
#import "UIViewController+AddPhoto.h"
#import "SlidingViewController.h"
#import "ImageHelper.h"
#import "SettingsTVC.h"
#import "ColorHelper.h"
#import "User+Create.h"

@interface SettingsTVC ()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *emailCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *facebookCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *twitterCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *resetPasswordCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *privacyPolicyCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *legalCell;

@end

@implementation SettingsTVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [ColorHelper tropicalBlue];
    
    [self setupStaticCells];
}

- (void)setupStaticCells {
    User *currentUser = [User currentUser];
    if (currentUser) {
        self.thumbnailView.image = [User thumbnailOfUser:currentUser];
        self.usernameLabel.text = currentUser.username;
        self.emailCell.textLabel.text = currentUser.email;
    } else {
        self.usernameLabel.text = @"No User";
        self.emailCell.textLabel.text = @"";
    }
}

#pragma mark - Actions

- (IBAction)changePictureButton {
    if (self.context) [self showActionSheet];
}

- (void)pickedPhoto:(UIImage *)photo {
    photo = [ImageHelper faceCenteredSquareImage:photo];
    NSData *photoData = UIImageJPEGRepresentation(photo, 1.0);
    [User currentUser].thumbnailData = photoData;
    self.thumbnailView.image = photo;
    [[NSNotificationCenter defaultCenter] postNotificationName:UserInfoChangedNotification
                                                        object:self userInfo:nil];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)logOutButton {
}

- (IBAction)showTutorialButton {
    SlidingViewController *svc = (SlidingViewController *)[self slidingViewController];
    [svc showTutorialIfNeeded:YES];
}

@end
