//
//  AddClubVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/5/14.
//
//

#import "UserListVC.h"

@interface AddUsersToClubVC : UserListVC

@property (nonatomic) BOOL editingExistingClub;
@property (strong, nonatomic) Club *club;

- (UITextField *)clubNameTextField;
- (NSMutableArray *)newMembers;
- (NSMutableArray *)newAdmins;

@end
