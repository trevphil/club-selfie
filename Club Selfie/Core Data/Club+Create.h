//
//  Club+Create.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Club.h"

@interface Club (Create)

+ (Club *)createClubInContext:(NSManagedObjectContext *)context;

@end
