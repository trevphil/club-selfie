//
//  NewClubInfoVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/10/14.
//
//

#import "NewClubInfoVC.h"

@interface NewClubInfoVC ()

@end

@implementation NewClubInfoVC

#pragma mark - Subclass Overrides

- (int)timeLeft {
    return [self.datePicker.date timeIntervalSinceNow];
}

- (BOOL)isAdmin {
    return YES;
}

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                              target:self
                                              action:@selector(doneButtonPressed:)];
    
    // setup date picker
    NSDate *minDate = self.datePicker.date;
    self.datePicker.minimumDate = minDate;
    int oneWeekInSeconds = 7 * 24 * 3600;
    NSDate *maxDate = [minDate dateByAddingTimeInterval:oneWeekInSeconds];
    self.datePicker.maximumDate = maxDate;
    self.datePicker.date = [[NSDate date] dateByAddingTimeInterval:3600];
    [self setupTimer];
    [self updateCountdown];
}

#pragma mark - Actions

- (void)doneButtonPressed:(UIBarButtonItem *)sender {
    // create club
    self.club = [NSEntityDescription insertNewObjectForEntityForName:@"Club"
                                              inManagedObjectContext:[User currentUser].managedObjectContext];
    self.club.unique = [@([NSDate timeIntervalSinceReferenceDate]) stringValue];
    self.club.createdAt = [NSDate date];
    self.club.lastActiveAt = [NSDate date];
    if ([self.clubName length]) {
        self.club.title = self.clubName;
    } else {
        self.club.title = [NSString stringWithFormat:@"%@'s Club", [User currentUser].username];
    }
    [self.club addMembers:self.clubMembers];
    [self.club addAdministrators:self.clubAdmins];
    
    // create objective
    self.objective = [NSEntityDescription insertNewObjectForEntityForName:@"Objective"
                                                   inManagedObjectContext:[User currentUser].managedObjectContext];
    self.objective.unique = [@([NSDate timeIntervalSinceReferenceDate]) stringValue];
    self.objective.createdAt = [NSDate date];
    self.objective.modifiedAt = [NSDate date];
    self.objective.expiresAt = self.datePicker.date;
    self.objective.club = self.club;
    if ([self.objectiveTextView.text length]) {
        self.objective.text = self.objectiveTextView.text;
    } else {
        self.objective.text = @"No objective";
    }
    
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

@end
