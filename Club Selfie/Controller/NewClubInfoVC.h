//
//  NewClubInfoVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/10/14.
//
//

#import "InfoViewController.h"

@interface NewClubInfoVC : InfoViewController

@property (strong, nonatomic) NSString *clubName;
@property (strong, nonatomic) NSSet *clubMembers;
@property (strong, nonatomic) NSSet *clubAdmins;

@end
