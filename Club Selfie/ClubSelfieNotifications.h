//
//  CustomNotifications.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/24/14.
//
//

#ifndef Club_Selfie_ClubSelfieNotifications_h
#define Club_Selfie_ClubSelfieNotifications_h

// to set the NSManagedObjectContext for Core Data
#define DatabaseAvailabilityNotification @"DatabaseAvailabilityNotification"
#define DatabaseAvailabilityContext @"Context"

#define CurrentClubDeletedNotification @"CurrentClubDeletedNotification"

#define UserInfoChangedNotification @"UserInfoChangedNotification"
#define CurrentUserAvailableNotification @"Current User Available"

#endif
