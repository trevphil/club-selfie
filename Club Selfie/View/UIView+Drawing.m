//
//  UIView+Drawing.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

#import "UIView+Drawing.h"

@implementation UIView (Drawing)

#pragma mark - Transformation

- (void)pushContextAndTransform:(void (^)(CGContextRef context))transform {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    transform(context);
}

- (void)popContext {
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

- (void)commonSetup {
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

@end
