//
//  AddPhotoViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/14/14.
//
//

#import "PostPhotoVC.h"
#import "User+Create.h"

@interface PostPhotoVC () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end

@implementation PostPhotoVC

#pragma mark - Actions

- (IBAction)cancel:(UIBarButtonItem *)sender {
    self.photo = nil; // clean up temporary files
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    [self.titleTextField resignFirstResponder];
}

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.photoView.contentMode = UIViewContentModeScaleAspectFit;
    self.photoView.image = self.photo;
    self.titleTextField.delegate = self;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Navigation

// (Done button)
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self.titleTextField resignFirstResponder];
    if (self.photo && self.club) {
        Post *post = [Post createPostInContext:self.club.managedObjectContext
                                      withData:UIImageJPEGRepresentation(self.photo, 1.0)];
        post.poster = [User currentUser];
        post.club = self.club;
        post.aspectRatio = @(self.photo.size.width / self.photo.size.height);
        post.title = [self.titleTextField.text length] ? self.titleTextField.text : @"";
        self.post = post;
    }
    self.photo = nil;
}

@end
