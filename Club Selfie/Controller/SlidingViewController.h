//
//  SlidingViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/10/14.
//
//

#import "ECSlidingViewController.h"

@interface SlidingViewController : ECSlidingViewController

- (void)showTutorialIfNeeded;
- (void)showTutorialIfNeeded:(BOOL)force;

@end
