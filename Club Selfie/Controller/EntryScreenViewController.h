//
//  EntryScreenViewController.h
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//  
//

#import <UIKit/UIKit.h>

@interface EntryScreenViewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *context;

@end
