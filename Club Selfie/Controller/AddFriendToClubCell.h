//
//  AddFriendToClubCell.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/6/14.
//
//

#import <UIKit/UIKit.h>
#import "CustomButton.h"

@interface AddFriendToClubCell : UITableViewCell

@property (weak, nonatomic, readonly) UIImageView *thumbnailView;
@property (strong, nonatomic) UIImage *thumbnail;
- (UILabel *)usernameLabel;
- (CustomButton *)addedButton;
- (CustomButton *)adminButton;

@end
