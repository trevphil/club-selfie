//
//  Post+Create.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Post.h"

@interface Post (Create)

+ (Post *)createPostInContext:(NSManagedObjectContext *)context;
+ (Post *)createPostInContext:(NSManagedObjectContext *)context
                     withData:(NSData *)data;

@end
