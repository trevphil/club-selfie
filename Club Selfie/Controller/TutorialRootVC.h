//
//  TutorialRootVC.h
//  Club Selfie
//
//  Created by Trevor Phillips on 8/15/14.
//
//

#import <UIKit/UIKit.h>

@interface TutorialRootVC : UIViewController <UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageImages;

@end
