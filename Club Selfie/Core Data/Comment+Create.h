//
//  Comment+Create.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/6/14.
//
//

#import "Comment.h"

@interface Comment (Create)

+ (Comment *)createComment:(NSString *)text inContext:(NSManagedObjectContext *)context;
+ (Comment *)createCommentInContext:(NSManagedObjectContext *)context;

@end
