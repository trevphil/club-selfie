//
//  UIView+Drawing.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

#import <UIKit/UIKit.h>

@interface UIView (Drawing)

- (void)pushContextAndTransform:(void (^)(CGContextRef context))transform;
- (void)popContext;
- (void)commonSetup;

@end
