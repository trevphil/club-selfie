//
//  CommentCell.m
//  DynamicTable
//
//  Created by Trevor Phillips on 7/14/14.
//  Copyright (c) 2014 Metova. All rights reserved.
//

#import "CommentCell.h"

@interface CommentCell ()

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailView;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end

@implementation CommentCell

- (UIImage *)thumbnail {
    return self.thumbnailView.image;
}

- (void)setThumbnail:(UIImage *)thumbnail {
    self.thumbnailView.image = thumbnail;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];
    self.commentLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.commentLabel.frame);
}

@end
