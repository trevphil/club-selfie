//
//  ClubSelfieLogo.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/20/14.
//
//

#import "ClubSelfieLogo.h"
#import "ColorHelper.h"

@implementation ClubSelfieLogo

- (void)setup {
    UILabel *appName = [[UILabel alloc] init];
    CGFloat fontSize = self.frame.size.height * 0.95;
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont fontWithName:@"OleoScript-Bold" size:fontSize],
                                  NSForegroundColorAttributeName : [ColorHelper mediumGreen],
                                  NSStrokeColorAttributeName : [UIColor blackColor],
                                  NSStrokeWidthAttributeName : @(-1) };
    appName.attributedText = [[NSAttributedString alloc] initWithString:@"Club Selfie"
                                                             attributes:attributes];
    appName.textAlignment = NSTextAlignmentCenter;
    
    [appName sizeToFit];
    CGRect bugFix = appName.frame;
    bugFix.size.width += 5.0f;
    appName.frame = bugFix;
    
    appName.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    [self addSubview:appName];
}

- (void)awakeFromNib {
    [self setup];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

@end
