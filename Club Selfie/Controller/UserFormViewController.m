//
//  UserFormViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "UserFormViewController.h"

@interface UserFormViewController () <UITextFieldDelegate>

@end

@implementation UserFormViewController

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.errorsTextLabel.text = @"";
}

#pragma mark - Submitting Data

- (IBAction)goButton {
    [self submitForm];
}

- (void)submitForm {
    // override in subclasses
}

#pragma mark - Validating User Input

- (void)displayError:(NSString *)msg {
    self.errorsTextLabel.text = msg;
    for (UITextField *textField in self.inputFields) {
        [textField resignFirstResponder];
    }
}

- (void)alertWithTitle:(NSString *)title message:(NSString *)msg {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

- (BOOL)validEmail:(NSString *)emailAddress {
    BOOL emailIsValid = YES;
    if ([emailAddress length]) {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        emailIsValid = [emailTest evaluateWithObject:emailAddress];
        if (!emailIsValid) [self displayError:@"Invalid email address."];
    } else {
        [self displayError:@"You must enter an email address."];
        emailIsValid = NO;
    }
    return emailIsValid;
}

- (BOOL)allFieldsAreValid {
    // override in subclasses
    return YES;
}

@end
