//
//  UIViewController+AddPhoto.h
//  Club Selfie
//
//  Created by Trevor Phillips on 7/17/14.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (AddPhoto) <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)showActionSheet;
- (void)pickedPhoto:(UIImage *)photo;

@end
