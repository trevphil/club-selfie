//
//  FriendsVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/15/14.
//
//

#import "CustomButton.h"
#import "ColorHelper.h"
#import "FriendCell.h"
#import "FriendsVC.h"

@interface FriendsVC ()

@property (strong, nonatomic) IBOutlet UIView *socialMediaView;
@property (weak, nonatomic) IBOutlet UIButton *facebookButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterButton;

@end

@implementation FriendsVC

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Friends";
    self.navigationController.navigationBar.barTintColor = [ColorHelper gold];
    [self setupSocialMediaView];
}

- (void)setupSocialMediaView {
    self.facebookButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.facebookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.facebookButton.layer.cornerRadius = 5.0f;
    
    self.twitterButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.twitterButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.twitterButton.layer.cornerRadius = 5.0f;

    CGSize preferredSize = [self.socialMediaView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    CGRect frame = self.socialMediaView.frame;
    frame.size = preferredSize;
    self.socialMediaView.frame = frame;
    self.tableView.tableHeaderView = self.socialMediaView;
}

#pragma mark - Actions

- (IBAction)addFriendButtonPressed:(UIButton *)sender {
    FriendCell *cell = (FriendCell *)[self superviewOfView:sender
                                                                    forClass:[FriendCell class]
                                                            currentIteration:0];
    UITableView *tableView = self.searchDisplayController.isActive ?
    self.searchDisplayController.searchResultsTableView : self.tableView;
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    User *user = self.searchDisplayController.isActive ?
    self.searchResults[indexPath.row] : [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([[User currentUser].friends containsObject:user]) {
        [[User currentUser] removeFriendsObject:user];
    } else {
        [[User currentUser] addFriendsObject:user];
    }
    
    [tableView reloadRowsAtIndexPaths:@[indexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)createSearchCell {
    return [self.tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
}

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user {
    FriendCell *friendCell = (FriendCell *)cell;
    friendCell.titleLabel.text = user.username;
    friendCell.thumbnail = [User thumbnailOfUser:user forSize:friendCell.thumbnailView.frame.size];
    BOOL isFriend = [[User currentUser].friends containsObject:user];
    friendCell.addFriendButton.chosen = isFriend;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
    return cell.frame.size.height;
}

@end
