//
//  PhotoButtonOrnament.m
//  Club Selfie
//
//  Created by Trevor Phillips on 7/8/14.
//
//

#import "PhotoButtonOrnament.h"
#import "UIView+Drawing.h"
#import "ColorHelper.h"

@interface PhotoButtonOrnament ()

@end

@implementation PhotoButtonOrnament

#pragma mark - Drawing

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // draw left side
    [self drawOrnamentInRect:rect inContext:context];
    
    // flip and draw right side
    [self pushContextAndTransform:^(CGContextRef context) {
        CGContextTranslateCTM(context, self.bounds.size.width, 0);
        CGContextScaleCTM(context, -1.0, 1.0);
    }];
    [self drawOrnamentInRect:rect inContext:context];
    [self popContext];
}

- (void)drawOrnamentInRect:(CGRect)rect inContext:(CGContextRef)context {
    CGContextSaveGState(context);
    
    CGPoint center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    UIBezierPath *ornament = [UIBezierPath bezierPathWithArcCenter:center
                                                            radius:rect.size.height / 2.0
                                                        startAngle:M_PI_2
                                                          endAngle:3.0 * M_PI_2
                                                         clockwise:YES];
    CGFloat halfWidth = rect.size.width / 2.0;
    CGFloat largeOffset = 0.62 * halfWidth;
    CGFloat smallOffset = 0.25 * halfWidth;
    CGPoint cp1 = CGPointMake(halfWidth - smallOffset, 0);
    CGPoint cp2 = CGPointMake(largeOffset, rect.size.height);
    CGPoint endPoint = CGPointMake(0, rect.size.height);
    [ornament addCurveToPoint:endPoint controlPoint1:cp1 controlPoint2:cp2];
    [ornament closePath];
    
    [[ColorHelper mediumGreen] setFill];
    [ornament fill];
    
    CGContextRestoreGState(context);
}

#pragma mark - Initialization

- (void)awakeFromNib {
    [self commonSetup];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonSetup];
    }
    return self;
}

@end
