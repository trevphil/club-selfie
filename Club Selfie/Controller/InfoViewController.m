//
//  InfoViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/10/14.
//
//

#import "InfoViewController.h"

@interface InfoViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *countdownLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *objectiveLabel;
@property (weak, nonatomic) IBOutlet UITextView *objectiveTextView;
@property (strong, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerHeight;
@property (weak, nonatomic) IBOutlet UIButton *changeButton;

@end

@implementation InfoViewController

#pragma mark - View Controller Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Info";
    
    // setup attributed text for "Objective" label
    self.objectiveLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Challenge:"
                                                                         attributes:@{ NSFontAttributeName : [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline] }];
    
    // setup timer and label
    [self setupTimer];
    [self updateCountdown];
    
    // dismiss the keyboard for the text view
    self.dismissKeyboardGesture = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:self.dismissKeyboardGesture];
    
    // setup objective description text view
    self.objectiveTextView.delegate = self;
    self.objectiveTextView.editable = self.isAdmin;
    
    // setup/hide date picker
    self.datePickerOriginalHeight = self.datePickerHeight.constant;
    [self switchDatePickerState:YES];
    self.changeButton.hidden = !self.isAdmin;
}

- (void)dismissKeyboard {
    [self.objectiveTextView resignFirstResponder];
}

#pragma mark - Properties

- (void)setObjective:(Objective *)objective {
    _objective = objective;
    [self updateFetchedResultsController];
}

// for subclasses
- (void)updateFetchedResultsController {
}

#pragma mark - Actions

- (IBAction)changedTimer:(UIDatePicker *)sender {
    [self setupTimer];
    [self updateCountdown];
    if (self.objective) {
        self.objective.expiresAt = self.datePicker.date;
    }
}

- (IBAction)changeCountdownButton:(UIButton *)sender {
    [self.objectiveTextView resignFirstResponder];
    [self switchDatePickerState:!self.datePicker.isHidden];
}

- (void)switchDatePickerState:(BOOL)hidden {
    self.datePicker.hidden = hidden;
    if (self.datePicker.isHidden) {
        self.datePickerHeight.constant = 0;
        [self.changeButton setTitle:@"Change" forState:UIControlStateNormal];
    } else {
        self.datePickerHeight.constant = self.datePickerOriginalHeight;
        [self.changeButton setTitle:@"Done" forState:UIControlStateNormal];
    }
    [self updateHeaderViewHeight];
}

// for subclasses
- (void)updateHeaderViewHeight {
}

#pragma mark - Timer

// for subclasses
- (int)timeLeft {
    return 0;
}

- (void)setupTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateCountdown)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)updateCountdown {
    int timeLeft = [self timeLeft];
    self.countdownLabel.textColor = timeLeft < 3600 ? [UIColor redColor] : [UIColor blackColor];
    if (timeLeft > 0) {
        NSString *timeLeftString = nil;
        float daysLeft = (float)timeLeft / (3600 * 24);
        if (daysLeft >= 1) {
            timeLeftString = [NSString stringWithFormat:@"%@ day%@", @([self roundNum:daysLeft toDecimals:1]),
                              [self roundNum:daysLeft toDecimals:1] == 1.0 ? @"" : @"s"];
        } else {
            int hoursLeft = timeLeft / 3600;
            int minutesLeft = (timeLeft - (3600 * hoursLeft)) / 60;
            int secondsLeft = timeLeft - (3600 * hoursLeft) - (60 * minutesLeft);
            if (minutesLeft > 0 || hoursLeft > 0) {
                timeLeftString = [NSString stringWithFormat:@"%02d", secondsLeft];
                timeLeftString = [NSString stringWithFormat:@"%02d:%@", minutesLeft, timeLeftString];
            } else {
                timeLeftString = [NSString stringWithFormat:@"%d", secondsLeft];
            }
            if (hoursLeft > 0) {
                timeLeftString = [NSString stringWithFormat:@"%02d:%@", hoursLeft, timeLeftString];
            }
        }
        timeLeftString = [@"Challenge Expires In:\n" stringByAppendingString:timeLeftString];
        self.countdownLabel.text = timeLeftString;
        NSMutableAttributedString *attrString = [self.countdownLabel.attributedText mutableCopy];
        [attrString setAttributes:@{ NSForegroundColorAttributeName : [UIColor blackColor] }
                            range:NSMakeRange(0, [@"Challenge Expires In:" length])];
        self.countdownLabel.attributedText = attrString;
    } else {
        [self.timer invalidate];
        self.timer = nil;
        self.countdownLabel.text = @"Time's up!";
    }
}

- (float)roundNum:(float)num toDecimals:(int)decimals {
    int tenpow = 1;
    for (int i = 0; i < decimals; i++) {
        tenpow *= 10;
    }
    return round(tenpow * num) / tenpow;
}

@end
