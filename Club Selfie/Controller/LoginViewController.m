//
//  LoginViewController.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/18/14.
//
//

#import "LoginViewController.h"

@interface LoginViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *emailUsernameLabel;
@property (weak, nonatomic) UITextField *emailUsernameTextField;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (nonatomic) BOOL loginTypeChosen;

@end

@implementation LoginViewController

#pragma mark - View Controller Lifecycle

- (void)awakeFromNib {
    [super awakeFromNib];
    // this is set before any prepareForSegue methods can to choose a login type
    self.loginTypeChosen = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.loginTypeChosen) {
        self.emailUsernameLabel.text = @"Email or Username";
    } else if (self.loggingInWithEmail) {
        self.emailUsernameLabel.text = @"Email";
    } else {
        self.emailUsernameLabel.text = @"Username";
    }
}

#pragma mark - Properties

- (void)setLoggingInWithEmail:(BOOL)loggingInWithEmail {
    _loggingInWithEmail = loggingInWithEmail;
    self.loginTypeChosen = YES;
}

#pragma mark - Form Submission

- (void)submitForm {
    if ([self allFieldsAreValid]) {
        if ([self useEmail:self.emailUsernameTextField.text]) {
            [self loginWithEmail:self.emailUsernameTextField.text
                    withPassword:self.passwordTextField.text];
        } else {
            [self loginWithUsername:self.emailUsernameTextField.text
                       withPassword:self.passwordTextField.text];
        }
    }
}

- (void)loginWithEmail:(NSString *)emailAddress withPassword:(NSString *)password {
    User *currentUser = [User userWithEmail:emailAddress password:password context:self.context];
    if (currentUser) {
        [[NSUserDefaults standardUserDefaults] setObject:currentUser.unique forKey:CURRENT_USER_ID_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:CurrentUserAvailableNotification object:nil];
        SlidingViewController *svc = (SlidingViewController *)self.presentingViewController;
        [svc dismissViewControllerAnimated:YES completion:^{
            [svc showTutorialIfNeeded];
        }];
    } else {
        [self displayError:@"Oops! Login failed."];
    }
}

- (void)loginWithUsername:(NSString *)username withPassword:(NSString *)password {
    User *currentUser = [User userWithUsername:username password:password context:self.context];
    if (currentUser) {
        [[NSUserDefaults standardUserDefaults] setObject:currentUser.unique forKey:CURRENT_USER_ID_KEY];
        [[NSNotificationCenter defaultCenter] postNotificationName:CurrentUserAvailableNotification object:nil];
        SlidingViewController *svc = (SlidingViewController *)self.presentingViewController;
        [svc dismissViewControllerAnimated:YES completion:^{
            [svc showTutorialIfNeeded];
        }];
    } else {
        [self displayError:@"Oops! Login failed."];
    }
}

#pragma mark - Text Fields

- (UITextField *)emailUsernameTextField {
    return self.inputFields[0];
}
- (UITextField *)passwordTextField {
    return self.inputFields[1];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    // the text fields' delegates should be set in the storyboard
    
    [textField resignFirstResponder];
    if ([textField isEqual:self.emailUsernameTextField]) {
        if ([self useEmail:self.emailUsernameTextField.text]) {
            if ([self validEmail:self.emailUsernameTextField.text]) {
                [self.passwordTextField becomeFirstResponder];
            }
        } else {
            if ([self validUsername:self.emailUsernameTextField.text]) {
                [self.passwordTextField becomeFirstResponder];
            }
        }
    } else { // we're editing the password, not email/username
        if ([self validPassword:self.passwordTextField.text]) {
            [self submitForm];
        }
    }
    
    return YES;
}

#pragma mark - Forgot Password

- (IBAction)forgotPasswordButton {
    UIAlertView *forgotPassword = [[UIAlertView alloc] initWithTitle:@"Reset Password"
                                                             message:@"A link will be sent to the email you provide for resetting your password."
                                                            delegate:self
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"Send", nil];
    forgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *alertTextField = [forgotPassword textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeEmailAddress;
    // auto-fill the alert's text field if the user has already entered an email address
    if (self.isLoggingInWithEmail || [self validEmail:self.emailUsernameTextField.text]) {
        alertTextField.text = self.emailUsernameTextField.text;
    }
    [forgotPassword show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView firstOtherButtonIndex]) {
        NSString *emailAddress = [alertView textFieldAtIndex:0].text;
        if ([self validEmail:emailAddress]) {
            [self resetPassword:emailAddress];
        }
    }
}

- (void)resetPassword:(NSString *)emailAddress {
    // provide an implementation here
}

#pragma mark - Validating User Input

- (BOOL)useEmail:(NSString *)emailOrUsername {
    if (!self.loginTypeChosen) {
        NSRange atSignRange = [emailOrUsername rangeOfString:@"@"];
        return atSignRange.location == NSNotFound ? NO : YES;
    } else {
        return self.loggingInWithEmail;
    }
}

- (BOOL)validUsername:(NSString *)username {
    if (![username length]) {
        [self displayError:@"You must enter a username."];
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)validPassword:(NSString *)password {
    if (![password length]) {
        [self displayError:@"You must enter a password."];
        return NO;
    } else {
        return YES;
    }
}

- (BOOL)allFieldsAreValid {
    BOOL emailUsernameGood;
    if ([self useEmail:self.emailUsernameTextField.text]) {
        emailUsernameGood = [self validEmail:self.emailUsernameTextField.text];
    } else {
        emailUsernameGood = [self validUsername:self.emailUsernameTextField.text];
    }
    
    if (emailUsernameGood && [self validPassword:self.passwordTextField.text]) {
        self.errorsTextLabel.text = @"";
        return YES;
    } else {
        return NO;
    }
}

@end
