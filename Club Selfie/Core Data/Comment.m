//
//  Comment.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/4/14.
//
//

#import "Comment.h"
#import "Post.h"
#import "User.h"


@implementation Comment

@dynamic createdAt;
@dynamic text;
@dynamic unique;
@dynamic commenter;
@dynamic post;

@end
