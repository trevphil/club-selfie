//
//  UIView+Styler.m
//  Club Selfie
//
//  Created by Trevor Phillips on 6/29/14.
//
//

#import "UIView+Styler.h"
#import "ColorHelper.h"

@implementation UIView (Styler)

#pragma mark - Colors

// flat colors
- (void)mediumGreenBackground {
    self.backgroundColor = [ColorHelper mediumGreen];
}

- (void)goldBackground {
    self.backgroundColor = [ColorHelper gold];
}

- (void)oceanBlueBackground {
    self.backgroundColor = [ColorHelper oceanBlue];
}

- (void)tropicalBlueBackground {
    self.backgroundColor = [ColorHelper tropicalBlue];
}

// dark to light gradient
- (void)bambooGradientBackground {
    [self gradientWithFirstColor:[ColorHelper bambooGradient][0]
                     secondColor:[ColorHelper bambooGradient][1]];
}

#pragma mark - Helper Methods

- (void)gradientWithFirstColor:(UIColor *)first
                   secondColor:(UIColor *)second {
    [self gradientWithFirstColor:first secondColor:second horizontal:NO];
}

- (void)gradientWithFirstColor:(UIColor *)first
                   secondColor:(UIColor *)second
                    horizontal:(BOOL)hor {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    if (hor) {
        gradient.startPoint = CGPointMake(0.0, 0.5);
        gradient.endPoint = CGPointMake(1.0, 0.5);
    }
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[first CGColor], (id)[second CGColor], nil];
    [self.layer insertSublayer:gradient atIndex:0];
}

#pragma mark - Corners

- (void)roundCorners {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 6.0f;
}

#pragma mark - Edges

- (void)borderWithColor:(UIColor *)color {
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [color CGColor];
}

- (void)noBorder {
    self.layer.borderWidth = 0;
    self.layer.borderColor = nil;
}


@end
