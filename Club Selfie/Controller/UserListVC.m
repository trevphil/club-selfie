//
//  UserListVC.m
//  Club Selfie
//
//  Created by Trevor Phillips on 8/5/14.
//
//

#import "UserListVC.h"

@interface UserListVC ()

@end

@implementation UserListVC

- (UIView *)superviewOfView:(UIView *)view forClass:(__unsafe_unretained Class)aClass
           currentIteration:(NSUInteger)iter {
    static NSInteger maxIterations = 10;
    if (iter > maxIterations) return nil;
    
    if ([view.superview isKindOfClass:aClass]) {
        return view.superview;
    } else {
        return [self superviewOfView:view.superview forClass:aClass currentIteration:(iter + 1)];
    }
}

#pragma mark - Properties

- (void)setContext:(NSManagedObjectContext *)context {
    _context = context;
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:[self createFetchRequest]
                                                                        managedObjectContext:self.context
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
}

#pragma mark - Search

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope {
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"username CONTAINS[c] %@", searchText];
    self.searchResults = [self.fetchedResultsController.fetchedObjects filteredArrayUsingPredicate:resultPredicate];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:self.tableView]) {
        return [super tableView:tableView numberOfRowsInSection:section];
    } else {
        return [self.searchResults count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:USER_CELL_IDENTIFIER];
    
    if (!cell) {
        cell = [self createSearchCell];
    }
    
    User *user = nil;
    if ([tableView isEqual:self.tableView]) {
        user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    } else {
        user = [self.searchResults objectAtIndex:indexPath.row];
    }
    [self configureCell:cell forUser:user];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell forUser:(User *)user {
    // for subclasses
}

- (UITableViewCell *)createSearchCell {
    // for subclasses (if they use custom cells)
    return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:USER_CELL_IDENTIFIER];
}

#pragma mark - Core Data

- (NSFetchRequest *)createFetchRequest {
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    request.predicate = [NSPredicate predicateWithFormat:@"%@ CONTAINS SELF", [User currentUser].knows];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"username"
                                                              ascending:YES
                                                               selector:@selector(localizedStandardCompare:)]];
    return request;
}

@end
